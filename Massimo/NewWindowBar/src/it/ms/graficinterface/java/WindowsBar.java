package it.ms.graficinterface.java;

import java.awt.Color;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JSeparator;

public class WindowsBar extends JFrame implements ActionListener{
	
	ImageIcon folder;
	ImageIcon save;
	ImageIcon exit;
		
	public WindowsBar(String titolo) {
		JMenuBar menubar = new JMenuBar();
		
		JMenu mImpostazioni = new JMenu("Impostazioni");
		JMenu mInformazioni = new JMenu("Informazioni");
		JMenu mFile = new JMenu("File");
		JMenuItem miApri = new JMenuItem("Apri");
		JMenuItem miSalva = new JMenuItem("Salva");
		JMenuItem mSalvaCon = new JMenuItem("Salva con nome");
		JMenuItem mChiudi = new JMenuItem("Chiudi");
		
		
		setTitle(titolo);
		setSize(800,600);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		menubar.add(mFile);
		menubar.add(mImpostazioni);
		menubar.add(mInformazioni);
		setJMenuBar(menubar);
		setVisible(true);
		
		mFile.add(miApri);
		miApri.addActionListener(this);
		mFile.add(new JSeparator());
		
		mFile.add(miSalva);
		miSalva.addActionListener(this);
		mFile.add(new JSeparator());
	
		mFile.add(mSalvaCon);
		mSalvaCon.addActionListener(this);
		mFile.add(new JSeparator());
		
		mFile.add(mChiudi);
		mChiudi.addActionListener(this);
		
		//Impostazioni Layout Finestra
		ImageIcon image = new ImageIcon("win.png");//Crea un ImageIcon
		
		//folder = new ImageIcon("folder.png");
		//save = new ImageIcon("win.png");
		//exit = new ImageIcon("exit.jpg");
		//miSalva.setIcon(save);
		
		this.setIconImage(image.getImage());
		this.setSize(600, 400);
		this.getContentPane().setBackground(Color.DARK_GRAY);
		
		
		
	}
	
		
	@Override
	public void actionPerformed(ActionEvent ae) {
		JMenuItem itemClicked = (JMenuItem) ae.getSource();
		String scritta = itemClicked.getText();//CLICCA SU SCRITTA 

		System.out.println("Hai cliccato " + scritta);
	
		if ("About".equals(scritta)) {
			JOptionPane.showMessageDialog(this, "Mia Applicazione v. 1.0.0");
		} else if ("Chiudi".equals(scritta)) {
			System.exit(0);
		}
		
		
	}

}

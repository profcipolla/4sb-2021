package it.interfacciegrafiche.java.ms;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JSeparator;


public class ApriFinestra extends JFrame implements ActionListener {
		
	/*	
	JButton myButton = new JButton("Clicca qui");
	*/
	public ApriFinestra() {
			Font myfont = new Font("Segoe UI",0,12);
			//Punto 1 creare una menu bar
			JMenuBar menubar = new JMenuBar();
			//Punto 2 creare i menu da visuallizare sulla menu bar
			JMenu mImpostazioni = new JMenu("Impostazioni");
			mImpostazioni.setFont(myfont);

			JMenu mInformazioni = new JMenu("Informazioni");
			mInformazioni.setFont(myfont);
			JMenu mFile = new JMenu("File");
			mFile.setFont(myfont);
			JMenu mApplicazioni = new JMenu("Applicazioni");
			mApplicazioni.setFont(myfont);
			
			JMenu mGiochi = new JMenu("Giochi");
			mGiochi.setFont(myfont);
			//Punto 3 creare i sotto menu da agganciare ai menu
			//Menu Item sottofinestre
			JMenuItem miApri = new JMenuItem("Apri");
			miApri.setFont(myfont);			
			JMenuItem miSalva = new JMenuItem("Salva");
			miSalva.setFont(myfont);		
			JMenuItem mSalvaCon = new JMenuItem("Salva con nome");
			mSalvaCon.setFont(myfont);		
			JMenuItem mChiudi = new JMenuItem("Chiudi");
			mChiudi.setFont(myfont);		
			JMenuItem mVersione = new JMenuItem("Versione");
			mVersione.setFont(myfont);		
			JMenuItem mAutore = new JMenuItem("Autore");
			mAutore.setFont(myfont);		
			
			
			
			//Settaggio della finestra
			setTitle("FINESTRA PRINCIPALE");
			setSize(800,600);
			setBounds(20,20,800,600);
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			setJMenuBar(menubar);//attacca il menu alla finestra
			
			this.setSize(600, 400);
			this.getContentPane().setBackground(Color.DARK_GRAY);
			//MENU AGGIUNTI SU BAR
			menubar.add(mFile);
			menubar.add(mApplicazioni);
			menubar.add(mGiochi);
			menubar.add(mInformazioni);
			
			JMenuItem miaFinestra = new JMenuItem("Prima Finestra");
			mApplicazioni.add(miaFinestra);
			miaFinestra.addActionListener(this);
			miaFinestra.setFont(myfont);
			
			JMenuItem miaFinestra2 = new JMenuItem("Seconda Finestra");
			mApplicazioni.add(miaFinestra2);
			miaFinestra2.addActionListener(this);
			miaFinestra2.setFont(myfont);
			/*
			myButton.setBounds(100, 160, 200, 40);
			myButton.setFocusable(false);
			myButton.addActionListener(this);
			menubar.add(myButton);
			*/
			
			mInformazioni.add(mVersione);
			mVersione.addActionListener(this);
			mInformazioni.add(new JSeparator());
			
			mInformazioni.add(mAutore);
			mAutore.addActionListener(this);
			
			mFile.add(miApri);
			miApri.addActionListener(this);
			mFile.add(new JSeparator());
			
			mFile.add(miSalva);
			miSalva.addActionListener(this);
			mFile.add(new JSeparator());
			
			mFile.add(mSalvaCon);
			mSalvaCon.addActionListener(this);
			mFile.add(new JSeparator());
			
			mFile.add(mChiudi);
			mChiudi.addActionListener(this);
			setVisible(true);
			
			ImageIcon image = new ImageIcon("win.png");//Crea un ImageIcon
			this.setIconImage(image.getImage());
	
	}
		
		@Override
		public void actionPerformed(ActionEvent e) {
			
			
			/*
			if(e.getSource()==myButton) {
				PrimaFinestra windows = new PrimaFinestra();
			}
			*/
			JMenuItem itemClicked = (JMenuItem) e.getSource();
			String scritta = itemClicked.getText();//CLICCA SU SCRITTA 
			if (itemClicked.getText().equalsIgnoreCase("Prima Finestra")) {
				new PrimaFinestra();
			}
			if (itemClicked.getText().equalsIgnoreCase("Seconda Finestra")) {
				new SecondaFinestra();
			}
			if ("Versione".equals(scritta)) {
				JOptionPane.showMessageDialog(this, "Mia Applicazione v.2.0");
			} else if ("Chiudi".equals(scritta)) {
				System.exit(0);
			}
			if("Autore".equals(scritta)) {
				JOptionPane.showMessageDialog(this, "Massimo Sammarco IVBS");
			}
		
			
			
		}
}




package it.interfacciegrafiche.java.ms;

import java.awt.Color;
import java.awt.Font;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class PrimaFinestra extends JFrame{
	Font myfont = new Font("Segoe UI",0,12);
	JFrame frame = new JFrame("PRIMA FINESTRA");
	ImageIcon image = new ImageIcon("win.png");
	
	JLabel label = new JLabel("FINESTRA 1");
	PrimaFinestra(){
		label.setBounds(40, 0, 100, 50);
		label.setFont(new Font(null,Font.PLAIN,13));
		frame.add(label);
		frame.setFont(myfont);
		frame.getContentPane().setBackground(Color.red);
		frame.setSize(420, 420);
		frame.setLayout(null);
		frame.setVisible(true);
		this.setIconImage(image.getImage());
		this.setSize(600, 400);
	}
}

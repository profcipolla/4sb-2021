
public class List<T> implements Lista<T> {
	private Nodo<T> head = null;

	public void add(T oggetto) {
		this.head = new Nodo <T>(oggetto,this.head);
		
	}

	public T get(int posizione) {
		Nodo<T> copia = this.head;
		while (null != copia && posizione != 0) {
			copia = copia.getSucc();
			posizione --;
		}
		if(null != copia) {
			return copia.getInfo();
		}
		return null;
	}
	@Override
	public String toString() {
		return this.head.toString();
	}
	

}

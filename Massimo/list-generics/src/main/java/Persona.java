import lombok.Data;

@Data
public class Persona {
	private String nome;
	private String cognome;
	private String codiceFiscale;
	public Persona(String nome, String cognome, String codiceFiscale) {
		setNome(nome);
		setCognome(cognome);
		setCodiceFiscale(codiceFiscale);
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public String getCodiceFiscale() {
		return codiceFiscale;
	}

	public void setCodiceFiscale(String codiceFiscale) {
		this.codiceFiscale = codiceFiscale;
	}

	@Override
	public String toString() {
		return String.format("Nome: %s. Cognome: %s. Codice Fiscale: %s", nome, cognome, codiceFiscale);
	}
	

}

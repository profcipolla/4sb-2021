import lombok.Data;

@Data
public class Libro {
	private String Autore;
	private String Titolo;
	
	public Libro(String Autore, String Titolo) {
		this.Autore = Autore;
		this.Titolo = Titolo;
		
	}

	public String getAutore() {
		return Autore;
	}

	public void setAutore(String autore) {
		Autore = autore;
	}

	public String getTitolo() {
		return Titolo;
	}

	public void setTitolo(String titolo) {
		Titolo = titolo;
	}


	@Override
	public String toString() {
		return String.format("Autore: %s,Titolo: %s", Autore,Titolo);
	}

	
	

}

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Nodo <T>{
	private T info;
	private Nodo <T> succ;
	
	public Nodo(T info) {
		this.info = info;
		this.succ = null;
	}

	public Nodo (T info,Nodo <T> succ) {
		this.info = info;
		this.succ = succ;
	}

	public T getInfo() {
		return info;
	}
	public void setInfo(T info) {
		this.info = info;
	}
	public Nodo<T> getSucc() {
		return succ;
	}

	public void setSucc(Nodo<T> succ) {
		this.succ = succ;
	}
	public String toString() {
		return this.info.toString();
	}

}

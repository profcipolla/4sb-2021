import lombok.Data;

@Data
public class Automobile {
	
	private String targa;
	private int anno;

}

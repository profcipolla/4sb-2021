
public class Auto
{
    private float consumoCarburante;
    private float quantitaCarburante = 0;
    
    public Auto(float consumoCarburante){
        this.consumoCarburante = consumoCarburante;
    }
    
    /*public void setConsumoCarburante(float consumoCarburante){
        this.consumoCarburante = consumoCarburante;
    }
    
    public float getConsumoCarburante(){
        return this.consumoCarburante;
    }*/
    
    public float dammiCarburante(){
        return this.quantitaCarburante;
    }
    
    public boolean drive(float distanzaKm){
        float consumo = this.consumoCarburante * distanzaKm;
        if (consumo > this.quantitaCarburante) {
            System.out.println("Ti servono : " + consumo + " litri");
            return false;
        } else {
            this.quantitaCarburante = this.quantitaCarburante - consumo;
            return true;
        }
    }
    
    public void faiRifornimento(float litri){
        this.quantitaCarburante = this.quantitaCarburante + litri;
    }
}

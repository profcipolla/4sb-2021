public class Autori
{
    protected String nome;
    protected String cognome;
    protected String dataDiNascita;
    protected String nazionalita;
    public Autori(String nome, String cognome, String dataDiNascita, String nazionalita){
        this.nome = nome;
        this.cognome = cognome;
        this.dataDiNascita = dataDiNascita;
        this.nazionalita = nazionalita;
    }
    
}

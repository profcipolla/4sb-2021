public class Libro
{
    protected String titolo;
    protected int anno;
    protected Autori autori;
    protected String codiceISBN;
    
    public Libro(Autori autori, int anno, String titolo, String codiceISBN){
        this.autori = autori;
        this.anno = anno;
        this.titolo = titolo;
        this.codiceISBN = codiceISBN;
    }
    
    public Libro(int anno, String titolo, String codiceISBN){
        this.autori = autori;
        this.anno = anno;
        this.titolo = titolo;
        this.codiceISBN = codiceISBN;
    }
}

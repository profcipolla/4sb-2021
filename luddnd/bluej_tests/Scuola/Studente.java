import java.util.Date;
/*
 * Nome
 * Cognome
 * Data di nascita
 * Matricola
 */
public class Studente extends Persona
{
    public int matricola;
    private Date dataDiNascita;
    
    public Studente(){
        this.nome = "Sconosciuto";
        this.cognome = "Sconosciuto";
    }
    
    public Studente (String nome, String cognome, int matricola, Date dataDiNascita) {
        this.nome = nome;
        this.cognome = cognome;
        this.matricola = matricola;
        this.dataDiNascita = dataDiNascita;
    }
}

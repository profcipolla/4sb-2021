
public abstract class Veicolo
{
    private String marca;
    private int anno;
    private int cilindrata;
    
    public Veicolo(String marca, int anno, int cilindrata){
        setMarca(marca);
        setAnno(anno);
        setCilindrata(cilindrata);
    }
    
    
    public String toString(){
        return "Marca: " + marca + " Anno: " + anno + " Cilindrata: " + cilindrata;
    }
    
    
    // getter-setter
    public String getMarca(){
        return marca;
    }
    
    public int getAnno(){
        return anno;
    }
    
    public int getCilindrata(){
        return cilindrata;
    }
    
    public void setMarca(String marca){
        this.marca = marca;
    }
    
    public void setAnno(int anno){
        this.anno = anno;
    }
    
    public void setCilindrata(int cilindrata){
        this.cilindrata = cilindrata;
    }
    
}


public class Auto extends Veicolo
{
    private int porte;
    private String alimentazione;    
    
    public Auto(String marca, int anno, int cilindrata, int porte, String alimentazione){
        super(marca, anno, cilindrata);
        setPorte(porte);
        setAlimentazione(alimentazione);       
    }
    
    public void setPorte(int porte){
        this.porte = porte;
    }
    
    public void setAlimentazione(String alimentazione){
        this.alimentazione = alimentazione;
    }
    
    public int getPorte(){
        return porte;
    }
    
    public String getAlimentazione(){
        return alimentazione;
    }
    
    public String toString(){
        return super.toString() + " Porte: " + getPorte() + " Alimentazione: " + getAlimentazione();
    }
    
    
}

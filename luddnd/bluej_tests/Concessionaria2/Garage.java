    
    public class Garage
    {
        private Veicolo[] posti = new Veicolo[15];
        
    public Veicolo estraiVeicolo(int posizione){
        Veicolo v = posti[posizione];
        posti[posizione] = null;
        return v;
    }
    
    public boolean immettiVeicolo(Veicolo veicolo){
        int pos = -1;
        int i = 0;
        while (i < 15 && pos == -1){
            if (posti[i] == null){
               pos = i; 
            }
            i++;
        }
        
        if (pos == -1){
            return false;
        }
        
        else {
            posti[pos] = veicolo;
            return true;
        }
    }
    
    public void stampa(){
        for (int i = 0; i < 15; i++){
            if (posti[i] != null){
            System.out.println(posti[i].toString());
            }
            else {
            System.out.println("Posizione " + (i+1) + " vuota");
            }
        }
    }
    
}

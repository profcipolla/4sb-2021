
public class Moto extends Veicolo
{
    private int peso, numeroCilindri;
    
    public Moto(String marca, int anno, int cilindrata, int peso, int numeroCilindri){
        super(marca, anno, cilindrata);
        setPeso(peso);
        setCilindri(numeroCilindri);
    }
    
    public void setPeso(int peso){
        this.peso = peso;
    }
    
    public int getPeso(){
        return peso;
    }
    
    public void setCilindri(int numeroCilindri){
        this.numeroCilindri = numeroCilindri;
    }
    
    public int getCilindri(){
        return numeroCilindri;
    }
    
    public String toString(){
        return super.toString() + " Peso: " + getPeso() + " Numero cilindri: " + getCilindri();
    }
}

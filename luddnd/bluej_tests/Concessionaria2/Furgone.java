public class Furgone extends Veicolo
{
    private int capacita;
    
    public Furgone(String marca, int anno, int cilindrata, int capacita){
        super(marca, anno, cilindrata);
        setCapacita(capacita);
    }
    
    public void setCapacita(int capacita){
        this.capacita = capacita;
    }
    
    public int getCapacita(){
        return this.capacita;
    }
    
    public String toString(){
        return super.toString() + " Capacita: " + this.getCapacita();
    }

}

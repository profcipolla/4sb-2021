
public class Studente
{
   private String nome;
   private String cognome;
   
   public Studente(String nome, String cognome){
       this.setNome(nome);
       this.setCognome(cognome);
   }
   
   public String getNome(){
       return this.nome;
   }
   
   public String getCognome(){
       return this.cognome;
   }
   
   public void setNome(String nome){
       if (null == nome){
           this.nome = "Sconosciuto";
       }
       else{
           this.nome = nome;
       }
   }
   
   public void setCognome(String cognome){
       if (null == cognome){
           this.cognome = "Sconosciuto";
       }
       else{
           this.cognome = cognome;
       }
   }
   
    @Override
    public boolean equals(Object other){
        if(!(other instanceof Studente)){
            return false;
        }
        Studente mc = (Studente) other;
        return this.nome.equals(mc.getNome()) && this.cognome.equals(mc.getCognome());
    }
}

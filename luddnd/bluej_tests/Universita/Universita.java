public class Universita
{
    private final int NUMERO_STUDENTI = 100;
    private final int NUMERO_DOCENTI = 20;
    private final int NUMERO_CORSI = 20;
    private String nome;
    private Sede sede;
    private Studente[] studenti = new Studente[NUMERO_STUDENTI];
    private Corsi[] corsi = new Corsi[NUMERO_CORSI];
    private Docenti[] docenti = new Docenti[NUMERO_DOCENTI];
    
    public Universita(String nome, Sede sede){
        this.nome = nome;
        this.sede = sede;
    }
    
    public Universita(String nome){
        this.setNome(nome);
    }
    
    public void setNome(String nome){
        if (null == nome){
            this.nome = "Sconosciuto";
        }
        else{
            this.nome = nome;
        }
    }
    
    public String getNome(){
        return this.nome;
    }
    
    public boolean aggiungiStudente(Studente studente){
        
        int i = 0;
        if (verificaStudente(studente) == true){
            return false;
        }
        while (this.studenti[i] != null && i < NUMERO_STUDENTI){
            i++;
        }
        if (i >= NUMERO_STUDENTI){
            return false;
        }
        this.studenti[i] = studente;
        return true;
    }
    
    public boolean aggiungiCorso(Corsi nuovoCorso){
        
        int i = 0;
        if (verificaCorso(nuovoCorso) == true){
            return false;
        }
        while (this.corsi[i] != null && i < NUMERO_CORSI){
            i++;
        }
        if (i >= NUMERO_CORSI){
            return false;
        }
        this.corsi[i] = nuovoCorso;
        return true;
    }
    
    public boolean aggiungiDocente(Docenti nuovoDocente){
        int i = 0;
        while (this.docenti[i] != null && i < NUMERO_DOCENTI){
            i++;
        }
        if (i >= NUMERO_DOCENTI){
            return false;
        }
        this.docenti[i] = nuovoDocente;
        return true;
    }
    
    public boolean aggiungiFrequenza(Corsi corso, Studente studente){
        return true;
    }
    
    public boolean aggiungiDocente(Corsi corso, Docenti docente){
        
        return true;
    }
    
    public boolean verificaStudente(Studente studente) {
        //ricerca in un array
        int i = 0;
        while (i < NUMERO_STUDENTI){
            if (this.studenti[i] == null){
                i++;
            } else {
                if (this.studenti[i].equals(studente)){
                    return true;
                }
                else{
                    i++;
                }
            }
        }
        return false;
    }
    
    public boolean verificaCorso(Corsi corso){
        //ricerca in un array
         int i = 0;
        while (i < NUMERO_CORSI){
            if (this.corsi[i] == null){
                i++;
            } else {
                if (this.corsi[i].equals(corso)){
                    return true;
                } else {
                    i++;
                }
            }
        }
        return false;
    }
    
    public int numeroStudenti(Corsi corso){
        
        return 0;
    }
    
}

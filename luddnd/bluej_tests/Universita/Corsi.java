
public class Corsi
{
    //public final int NUMERO_STUDENTI = 50;
    private int NUMERO_STUDENTI = 50;
    private String nome;
    private Studente[] studenti;
    private Docenti docente;
    
    
    public Corsi(String nome, Docenti docente, int NUMERO_STUDENTI){
        this.setNome(nome);
        this.setDocente(docente);
        this.NUMERO_STUDENTI = NUMERO_STUDENTI;
        this.studenti = new Studente[NUMERO_STUDENTI];
    }
   
    public void setDocente(Docenti docente){
        this.docente = docente;
    }
    
    public Docenti getDocente(){
        return this.docente;
    }
    
    public boolean setStudente(Studente studente, int posizione){
        if (this.studenti[posizione] != null){
            return false;
        }
        this.studenti[posizione] = studente;
        return true;
    }
    
    public Studente getStudente(int posizione){
        return this.studenti[posizione];
    }
    
    public void setNome(String nome){
        if (null == nome){
           this.nome = "Sconosciuto";
       }
       else{
           this.nome = nome;
       }
    }
    
    public String getNome(){
        return this.nome;
    }
    
    @Override
    public boolean equals(Object other){
        if(!(other instanceof Corsi)){
            return false;
        }
        Corsi mc = (Corsi) other;
        return this.nome.equals(mc.getNome());
    }
    
}

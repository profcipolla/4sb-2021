
public class Docenti
{
   private String nome;
   private String cognome;
   
   public Docenti(String nome, String cognome){
       this.setNome(nome);
       this.setCognome(cognome);
   }
   
   public String getNome(){
       return this.nome;
   }
   
   public void setNome(String nome){
       if (null == nome){
           this.nome = "Sconosciuto";
       }
       else{
           this.nome = nome;
       }
   }
   
   public void setCognome(String cognome){
       if (null == cognome){
           this.cognome = "Sconosciuto";
       }
       else{
           this.cognome = cognome;
       }
   }
}

public abstract class veicolo
{
    protected casaCostruttrice casaCostruttrice;
    protected String modello;
    protected int annoDiCostruzione;
    
    public veicolo(casaCostruttrice casaCostruttrice, String modello, int annoDiCostruzione){
        this.casaCostruttrice = casaCostruttrice;
        this.modello = modello;
        this.annoDiCostruzione = annoDiCostruzione;
    }
    
    
}

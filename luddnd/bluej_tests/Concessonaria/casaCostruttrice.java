
public class casaCostruttrice
{
    public String nome;
    public String nazione;
    public int annoFondazione;
    public int numeroConcessionariaInItalia;
    
    public casaCostruttrice(String nome, String nazione, int pAnnoFondazione, int pnumeroConcIta){
        this.nome = nome;
        this.nazione = nazione;
        annoFondazione = pAnnoFondazione;
        numeroConcessionariaInItalia = pnumeroConcIta;
    }
    
    public casaCostruttrice(String nome, String nazione){
        this.nome = nome;
        this.nazione = nazione;
    }
    
}

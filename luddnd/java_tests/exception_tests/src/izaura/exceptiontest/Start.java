package izaura.exceptiontest;

public class Start {

	public static void main(String[] args){
		Persona p = new Persona();
		
		try {
			p.setCodiceFiscale(null);
			System.out.print(p);
		} catch (CodiceFiscaleNullException exc) {
			System.err.println("Codice fiscale vuoto");
		} catch (CodiceFiscaleException exc) {
			System.err.println("Codice fiscale non valido");
		}
		
		
	}

}

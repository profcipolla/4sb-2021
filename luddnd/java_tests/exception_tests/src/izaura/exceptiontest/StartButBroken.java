package izaura.exceptiontest;

public class StartButBroken {

	public static void main(String[] args) throws CodiceFiscaleException {
		Persona p = new Persona();
		
		try {
			p.setCodiceFiscale("");
			System.out.print(p);
		} catch (CodiceFiscaleNullException exc) {
			System.err.println("Codice fiscale vuoto");
		}
		

	}

}

package izaura.exceptiontest;

public class Persona {
	private String nome;
	private String cognome;
	private String codiceFiscale;
	
	public void setCodiceFiscale(String codiceFiscale) throws CodiceFiscaleNullException, CodiceFiscaleException {
		if (codiceFiscale == null) {
			throw new CodiceFiscaleNullException();
		} 
		if (codiceFiscale.length() != 16) {
			throw new CodiceFiscaleException();
		}
		this.codiceFiscale = codiceFiscale;
	}

	@Override
	public String toString() {
		return "Persona [codiceFiscale=" + codiceFiscale + "]";
	}
	
	

}

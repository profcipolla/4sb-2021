package com.luddnd;

public class Main {

	public static void main(String[] args) {
		Persona p1 = new Persona("Mario", "Rossi", "yes");
		Persona p2 = new Persona("Tizio", "Quello", "AAAAAAAAAAAAAAAAA");
		
		Lista amici = new Lista();
		amici.add(p1);
		amici.add(p2);
		
		Persona p3 = new Persona("io", "Sta gente", "non la conosco");
		amici.add(p3);
		
		System.out.println(amici.toString());
		
		Persona sWee = amici.search("AAAAAAAAAAAAAAAAA");
		if (null == sWee) {
			System.out.println("Non ho trovato il tuo amico (Relatable)");
		} else {
			System.out.println("Dati dell'amico: \n " + sWee);
		}
	}

}

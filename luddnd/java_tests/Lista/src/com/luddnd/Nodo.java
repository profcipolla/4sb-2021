package com.luddnd;

public class Nodo {
	// Cambiare Object
	private Persona info;
	// S U C C
	private Nodo succ;
	
	public Nodo(Persona info) {
		this.info = info;
		this.succ = null;
	}
	
	public Nodo (Persona info, Nodo succ) {
		this.info = info;
		this.succ = succ;
	}

	public Persona getInfo() {
		return info;
	}

	public void setInfo(Persona info) {
		this.info = info;
	}

	public Nodo getSucc() {
		return succ;
	}

	public void setSucc(Nodo succ) {
		this.succ = succ;
	}

	@Override
	public String toString() {
		String result = String.format("Nome: %s, Cognome: %s, Codice Fiscale: %s \n", info.getNome(), info.getCognome(), info.getCodiceFiscale());
		if (this.succ != null) {
			result = result + this.succ.toString();
		}
		return result;
	}
	

}

package com.luddnd;

public class Lista{
	private Nodo head;
	
	public Lista() {
		this.head = null;
	}
	
	public void add (Persona p) {
		Nodo newNodo = new Nodo(p, this.head);
		this.head = newNodo;
	}
	public void remove (String key) {
		
		
	}
	
	public Persona search (String key) {
		if (null == this.head) {
			return null;
		}
		
		Nodo temp = this.head;
		while(temp != null && !key.equalsIgnoreCase(temp.getInfo().getCodiceFiscale())) {
			temp = temp.getSucc();
		}
		
		if (temp == null) {
			return null;
		}
		return temp.getInfo();
	}

	@Override
	public String toString() {
		return this.head.toString();
	}

}

package com.luddnd.lista.auto;
import com.luddnd.lista.interfaccia.*;


public class Lista implements ListaInterface {
	private Nodo head = null;
	private int size = 0;
	public void add(Veicolo newVeicolo) {
		
		Nodo newNodo = new Nodo(newVeicolo, this.head);
		this.head = newNodo;
		size++;
	}
	
	@Override
	public Veicolo get(int posizione) {
		Nodo copia = this.head;
		while (null != copia && posizione != 0) {
			copia = copia.getNext();
			posizione--;
		}
		
		if (null != copia) {
			return copia.getInfo();
		}
		return null;
	}
	
	@Override
	public boolean remove(String key) {
		if (null == this.head || null == key) {
			return false;
		}
		Nodo next = this.head;
		Nodo tempPrec = null;
		while (next != null && !key.equalsIgnoreCase(next.getInfo().getTarga())) {
			tempPrec = next;
			next = next.getNext();
		}
		
		if (next == null) {
			return false;
		}
		
		if (null == tempPrec) {
			this.head = this.head.getNext();
			size--;
			return true;
		}
		tempPrec.setNext(next.getNext());
		size--;
		return true;
		
	}
	
	@Override
	public Veicolo search(String key) {
		if (null == this.head) {
			return null;
		}
		
		Nodo temp = this.head;
		while(temp != null && !key.equalsIgnoreCase(temp.getInfo().getTarga())) {
			temp = temp.getNext();
		}
		
		if (temp == null) {
			return null;
		}
		return temp.getInfo();
	}
	
	@Override
	public String toString() {
		return this.head.toString();
	}
	
	@Override
	public int getSize() {
		return this.size;
	}
}

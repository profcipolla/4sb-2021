package com.luddnd.lista.auto;
import com.luddnd.lista.interfaccia.*;
public class Main {

	public static void main(String[] args) {
		//Lista() ListaAddFine() ListaOrdinata()
		ListaInterface miaListaCorrente = new ListaOrdinata();
		
		miaListaCorrente.add(new Camion("JF 55300"));
		miaListaCorrente.add(new Moto("BF 586201"));
		miaListaCorrente.add(new Automobile("Who?"));
		miaListaCorrente.add(new Veicolo("Da rimuovere"));
		miaListaCorrente.remove("Da Rimuovere");
		// miaListaCorrente.riordina();
		// System.out.println("Reverse");
		miaListaCorrente.reverse();
		
		System.out.println(String.format("Size: %s \n", miaListaCorrente.getSize()) + miaListaCorrente.toString());
	

	}

}

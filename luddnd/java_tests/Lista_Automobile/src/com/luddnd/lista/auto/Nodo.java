package com.luddnd.lista.auto;

public class Nodo {
	private Veicolo info;
	private Nodo next;
	private Nodo prec;
	
	public Nodo (Veicolo newObject, Nodo next) {
		this.info = newObject;
		this.next = next;
		this.prec = null;
	}
	
	public Nodo (Veicolo newObject, Nodo next, Nodo prec) {
		this.info = newObject;
		this.next = next;
		this.prec = prec;
	}
	
	// Getter-Setter
	
	public Nodo getPrec() {
		return prec;
	}


	public void setPrec(Nodo prec) {
		this.prec = prec;
	}

	
	public Veicolo getInfo() {
		return info;
	}
	public void setInfo(Veicolo info) {
		this.info = info;
	}
	public Nodo getNext() {
		return next;
	}
	public void setNext(Nodo next) {
		this.next = next;
	}
	public Nodo getLast() {
		Nodo tmp = this.next;
		Nodo last = null;
		if (tmp == null) {
			return null;
		} else {
			while (tmp != null) {
				last = tmp;
				tmp.getNext();
			}
			return last;
		}
	}
	
	@Override
	public String toString() {
		String result = this.info.toString();
		if (next != null) {
			result = result + "\n" + this.next.toString();
		}
		return result;
	}
	

}

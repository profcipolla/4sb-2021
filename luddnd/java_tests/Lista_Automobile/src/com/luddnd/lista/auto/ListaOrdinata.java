package com.luddnd.lista.auto;

import com.luddnd.lista.interfaccia.ListaInterface;

/* La Lista è un ADT (Abstrac Data Type)
 * è un contenitore simile and un array.
*/

public class ListaOrdinata implements ListaInterface {
	private Nodo head = null;
	private int size = 0;

	public void reverse() {
		if (this.size < 2) {
			//BREAK THIS
		}
		Nodo tmp = this.head.getNext();
		Nodo last = this.head;
		last.setNext(null);
		
		while (tmp != null) {
			Nodo appo = last;
			last = tmp;
			tmp = tmp.getNext();
			last.setNext(appo);
		}
		this.head = last;
		
	}


	@Override
	public void riordina() {
		// TODO Da Implementare
		Nodo start = this.head;
		if (compareTo(start, start.getNext()) > 0 && start.getNext() != null) {

		}
		this.head = start;
		for (int i = 0; i <= size - 1; i++) {
		}
	}

	public int compareTo(Nodo Nodo1, Nodo Nodo2) {
		return Nodo1.getInfo().getTarga().compareToIgnoreCase(Nodo2.getInfo().getTarga());
	}

	@Override
	public void add(Veicolo newVeicolo) {
		Nodo newNodo = new Nodo(newVeicolo, null);
		if (head == null) {
			head = newNodo;
		} else {
			Nodo tmp = head;
			Nodo last = null;
			while (tmp != null && tmp.getInfo().getTarga().compareTo(newNodo.getInfo().getTarga()) == 0) {
				last = tmp;
				tmp = tmp.getNext();
			}
			if (tmp == null) {
				last.setNext(newNodo);
			} else {
				newNodo.setNext(tmp);
				if (last == null) {
					head = newNodo;
				} else {
					last.setNext(newNodo);
				}

			}
		}
		size++;
	}

	@Override
	public boolean remove(String key) {
		if (null == this.head || null == key) {
			return false;
		}
		Nodo next = this.head;
		Nodo tempPrec = null;
		while (next != null && !key.equalsIgnoreCase(next.getInfo().getTarga())) {
			tempPrec = next;
			next = next.getNext();
		}

		if (next == null) {
			return false;
		}

		if (null == tempPrec) {
			this.head = this.head.getNext();
			size--;
			return true;
		}
		tempPrec.setNext(next.getNext());
		size--;
		return true;

	}

	@Override
	public Veicolo get(int posizione) {
		Nodo copia = this.head;
		while (null != copia && posizione != 0) {
			copia = copia.getNext();
			posizione--;
		}

		if (null != copia) {
			return copia.getInfo();
		}
		return null;
	}

	@Override
	public Veicolo search(String key) {
		if (null == this.head || null == key) {
			return null;
		}

		Nodo temp = this.head;
		while (temp != null && !key.equalsIgnoreCase(temp.getInfo().getTarga())) {
			temp = temp.getNext();
		}

		if (temp == null) {
			return null;
		}
		return temp.getInfo();
	}

	@Override
	public String toString() {
		return this.head.toString();
	}

	public int getSize() {
		return size;
	}

}

package com.luddnd.lista.interfaccia;

import com.luddnd.lista.auto.Veicolo;

public interface ListaInterface {
	void add(Veicolo veicolo);
	Veicolo get(int posizione);
	Veicolo search(String key);
	boolean remove(String key);
	int getSize();

}

package com.izaura.swingmenu;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

import com.izaura.wintris.WinTris;

public class Finestra extends JFrame implements ActionListener{
	private static final long serialVersionUID = 1L;

	public Finestra() {		
		initSize();
		JMenuBar menu = new JMenuBar();		
		JMenu file = new JMenu("File");
		JMenu applicazioni = new JMenu("Applicazioni");
		JMenu help = new JMenu("Aiuto");
		Font newfont = new Font("Segoe UI", 0, 20);
		menu.setFont(newfont);
		file.setFont(newfont);
		
		menu.add(file);
		menu.add(applicazioni);
		menu.add(help);
		
		
		// FILE
		JMenuItem apri = new JMenuItem("Apri");
		apri.addActionListener(this);
		JMenuItem salva = new JMenuItem("Salva");
		salva.addActionListener(this);
		JMenuItem salvaConNome = new JMenuItem("Salva con nome...");
		salvaConNome.addActionListener(this);
		JMenuItem esci = new JMenuItem("Esci");
		esci.addActionListener(this);
		esci.addActionListener(this);
		file.add(apri);
		file.add(salva);
		file.add(salvaConNome);
		file.add(new JSeparator());
		file.add(esci);
		
		// IMPOSTAZIONI
		JMenuItem wintris = new JMenuItem("WinTris");
		wintris.addActionListener(this);
		applicazioni.add(wintris);
		
		//HELP
		JMenuItem about = new JMenuItem("About...");
		about.addActionListener(this);
		help.add(about);
		
		
		setJMenuBar(menu);
		setVisible(true);
	}
	
	public void initSize() {
		// Inizializza finestra
		setSize(800,600);
		setTitle("MenuFigo");
		ImageIcon image = new ImageIcon("classpath:resources/icon.png");
		setIconImage(image.getImage());
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public void actionPerformed(ActionEvent e) {
		JMenuItem button = (JMenuItem)e.getSource();
		System.out.println(button.getText());
		
		if (button.getText().equals("About...")) {
			JOptionPane.showMessageDialog(this, "Heyooo..", "About", JOptionPane.INFORMATION_MESSAGE);
		}
		if (button.getText().equals("Apri")) {
			// Idk
		}
		if (button.getText().equals("Esci")) {
			dispose();
		}
		if (button.getText().equalsIgnoreCase("WinTris")) {
			new WinTris();
		}
	}

}

package com.izaura.wintris;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class WinTris extends JFrame implements ActionListener {

	// Non so a che serve questo
	private static final long serialVersionUID = 1L;

	private static final int DIM = 9;
	private JButton[] leButtons = new JButton[DIM];
	private boolean isX = true;
	private Font btnFont = new Font("Arial", Font.BOLD, 38);
	private Font outputFont = new Font("Segoe UI", Font.PLAIN, 24);
	private int[] vinto = new int[3];
	private JLabel info = new JLabel("Turno di: X");
	
	public WinTris() {
		initSize(400, 480, false);
		setTitle("WinTris");
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		mainWindow();
		setVisible(true);
	}
	
	public void initSize(int width, int height, boolean isResizable) {
		// Determina risoluzione schermo, e centra la finestra.
		// ((int) Dimensione schermo - grandezza finestra) / 2
		Toolkit screen = Toolkit.getDefaultToolkit();
		Dimension dim = screen.getScreenSize();
		setSize(width, height);
		int widthToUse = (((int) dim.getWidth() - getWidth()) / 2);
		int heightToUse = (((int) dim.getHeight() - getHeight()) / 2);
		setBounds(widthToUse, heightToUse, width, height);
		setResizable(isResizable);

	}

	public void mainWindow() {
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.PAGE_AXIS));
		JPanel trisPanel = new JPanel();
		trisPanel.setLayout(new FlowLayout());
		trisPanel.setMaximumSize(new Dimension(420,350));
		trisPanel.setPreferredSize(new Dimension(420,350));
		
		
		for (int i = 0; i <= DIM-1; i++) {
			leButtons[i] = new JButton();
			leButtons[i].setPreferredSize(new Dimension(100, 100));
			leButtons[i].setFont(this.btnFont);
			leButtons[i].addActionListener(this);
			leButtons[i].setText("");
			trisPanel.add(leButtons[i]);
		}
		
		JMenu menu = new JMenu("Gioco");
		JMenuItem newgame = new JMenuItem("Nuova Partita");
		JMenuItem esci = new JMenuItem("Esci");
		JMenuBar barra = new JMenuBar();
		esci.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
					dispose();
				}
			});
		
		
		newgame.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				for (int i = 0; i <= DIM-1; i++) {
					// Colore di default: 238-238-238
					leButtons[i].setBackground(null);
					leButtons[i].setText("");
					leButtons[i].setEnabled(true);
					info.setText("Turno di: X");
				}
			}
		});
		menu.add(newgame);
		menu.add(esci);
		barra.add(menu);
		setJMenuBar(barra);
		JPanel infoPanel = new JPanel();
		infoPanel.setMaximumSize(new Dimension(300,200));
		info.setFont(outputFont);
		infoPanel.add(info);
		mainPanel.add(trisPanel);
		mainPanel.add(infoPanel);
		add(mainPanel);
			
	}

	public void actionPerformed(ActionEvent aevent) {
		JButton temp = (JButton) aevent.getSource();
		if (!"".equals(temp.getText())) {
			return;
		}
		temp.setText(this.isX ? "X" : "O");
		this.info.setText(!this.isX ? "Turno di: X" : "Turno di: O");
		this.isX = !this.isX;
		if (getWinner(this.isX, 0,1,2) || getWinner(this.isX, 3,4,5) || getWinner(this.isX,6,7,8) || getWinner(this.isX,0,3,6) || getWinner(this.isX,1,4,7) || getWinner(this.isX,2,5,8) || getWinner(this.isX,0,4,8) || getWinner(this.isX,2,4,6)) {
			this.hoVinto();
		} else {
			this.hoPareggiato();
		}

	}
	
	public void hoPareggiato() {
		int i = 0;
		boolean check = false;
		while (i <= DIM-1 && !check) {
			check = this.leButtons[i].getText().equalsIgnoreCase("");
			i++;
		}
		if (i > 8 && !check) {
			for (i = 0; i <= DIM-1; i++) {
				this.leButtons[i].setEnabled(false);
			}
			this.isX = true;
			this.info.setText("Pari");
		} 
	}
	
	public void hoVinto() {
		String output = "X";
		for (int i = 0; i <= DIM-1; i++) {
			this.leButtons[i].setEnabled(false);
			this.leButtons[i].setBackground(new Color(0.9f, 0.0f, 0.0f, 0.7f));
		}
		Color win = new Color(0.0f, 0.9f, 0.0f, 1.0f);
		this.leButtons[this.vinto[0]].setBackground(win);
		this.leButtons[this.vinto[1]].setBackground(win);
		this.leButtons[this.vinto[2]].setBackground(win);
		if (this.isX) {
			output = "O";
		}
		this.info.setText(String.format("%s ha vinto", output));
		this.isX = true;
	}

	public boolean getWinner(Boolean isX, int cord1, int cord2, int cord3) {
		boolean vincita = false;
		String simbolo = "X";
		if (Boolean.TRUE.equals(isX)) {
			simbolo = "O";
		}
		if (leButtons[cord1].getText().equals(simbolo) && leButtons[cord2].getText().equals(simbolo)
				&& leButtons[cord3].getText().equals(simbolo)) {
			vincita = true;
			this.vinto[0] = cord1;
			this.vinto[1] = cord2;
			this.vinto[2] = cord3;
		}
		return vincita;
		
	}

}

package com.izaura.winmouse;

import java.awt.Font;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class WinMouse extends JFrame implements MouseListener, MouseMotionListener{
	
	private final int W = 600;
	private final int H = 400;
	
	private JButton btnOK = new JButton("ok");
	private JButton btnNO = new JButton("no");
	
	public WinMouse() {
		setTitle("ACME INC.");
		setBounds(50,50,W,H);
		setLayout(null);
		JLabel messaggio = new JLabel("<html> AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA<br>AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA<br>AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA<br>AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA </html>");
		messaggio.setFont(new Font("Segoe UI",0,20));
		messaggio.setBounds(10,10, W-200,60);
		btnOK.setBounds(W-230,H-80,80,30);
		btnOK.addMouseMotionListener(this);
		add(messaggio);
		add(btnOK);
		add(btnNO);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		setVisible(true);
	}
	
	public static void main(String[] args) {
		new WinMouse();
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseMoved(MouseEvent e) {
		int dx = (int) (Math.random() * 100) - 50;
		int dy = (int) (Math.random() * 100) - 50;
	}
}

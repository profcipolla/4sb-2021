package com.izaura.generics.mvn.pila;

public interface Pila<T>{
	void push(T info);
	T pop();
	
}

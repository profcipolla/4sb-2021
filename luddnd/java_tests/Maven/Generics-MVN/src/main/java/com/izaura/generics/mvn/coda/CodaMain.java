package com.izaura.generics.mvn.coda;

import com.izaura.generics.mvn.common.*;

// FIFO - First in, first out
public class CodaMain<T> implements Coda<T>{
	private Nodo<T> tail = null;

	@Override
	public void enqueue(T automobile) {
		this.tail = new Nodo<T>(automobile,this.tail);
	}

	@Override
	public T dequeue() {
		if (null == this.tail) {
			return null;
		}
		
		Nodo<T> tmp = this.tail;
		Nodo<T> prec = null;
		while (tmp.getSucc() != null) {
			prec = tmp;
			tmp = tmp.getSucc();
		}		
		
		T auto = tmp.getInfo();
		if (prec == null) {
			this.tail = null;
		} else {
			prec.setSucc(null);
		}
		return auto;
	}
}

package com.izaura.generics.mvn.lista;

public interface Lista<T> extends Iterable<T>{
	void add(T objectin);
	T get(int posizione);
	int getSize();
	void remove(int posizione);
}

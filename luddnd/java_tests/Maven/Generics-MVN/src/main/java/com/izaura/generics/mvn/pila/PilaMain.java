package com.izaura.generics.mvn.pila;

import com.izaura.generics.mvn.common.*;

public class PilaMain<T> implements Pila<T>{
	//LIFO - Last in First Out
	private Nodo<T> top = null;
	private int size = 0;

	@Override
	public T pop() {
		if (null == top) {
			return null;
		}
		T auto = this.top.getInfo();
		this.top = this.top.getSucc();
		size--;
		return auto;
	}

	@Override
	public void push(T automobile) {
		Nodo<T> newNodo = new Nodo<T>(automobile, this.top);
		this.top = newNodo;
		size++;
		
	}

}

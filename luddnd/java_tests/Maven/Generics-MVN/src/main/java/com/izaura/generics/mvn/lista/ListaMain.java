package com.izaura.generics.mvn.lista;

import java.util.Iterator;
import com.izaura.generics.mvn.common.*;

public class ListaMain<T> implements Lista<T>{
	private Nodo<T> head;
	private int size = 0;
	
	public ListaMain() {
		this.head = null;
	}
	
	public void add(T objectin) {
		Nodo<T> newNodo = new Nodo<>(objectin, this.head);
		this.head = newNodo;
		size++;
	}

	@Override
	public String toString() {
		return this.head.toString();
	}

	@Override
	public T get(int posizione) {
		Nodo<T> copia = this.head;
		while (null != copia && posizione != 0) {
			copia = copia.getSucc();
			posizione--;
		}
		
		if (null != copia) {
			return copia.getInfo();
		}
		return null;
	}

	
	public int getSize() {
		return this.size;
	}
	
	public void remove(int posizione) {
		if (posizione < 0 || posizione > size - 1) {
			return;
		}
		if (0 == posizione) {
			this.head = this.head.getSucc();
			this.size--;
			return;
		}
		
		Nodo<T> temp = this.head;
		int conta = 0;
		while (null != temp && conta != (posizione - 1)) {
			temp = temp.getSucc();
			conta++;
		}

		if (conta == (posizione - 1)) {
			if (null != temp.getSucc()) {
				temp.setSucc(temp.getSucc().getSucc());
			} else {
				this.head = null;
			}
			this.size--;
		}
	}

	@Override
	public Iterator<T> iterator() {
		return new IteratorList<>(this.head);
	}

	private class IteratorList<IG> implements Iterator<IG> {
		private Nodo<IG> nodoCorrente;
		
		public IteratorList(Nodo<IG> head) {
			nodoCorrente = head;
		}

		@Override
		public boolean hasNext() {
			return null != this.nodoCorrente;
		}

		@Override
		public IG next() {
			IG info = nodoCorrente.getInfo();
			this.nodoCorrente = this.nodoCorrente.getSucc();
			return info;
		} 
		
	}



}

package com.izaura.generics.mvn.main;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

public class AnagraficaPersonaNuovo extends JFrame implements ActionListener{
	private static final long serialVersionUID = 1L;
	private int width = 0;
	private int height = 0;
	private AnagraficaPersona parent = null;
	private JTextField textCognome = new JTextField(20);
	private JTextField textNome = new JTextField(20);
	private JTextField textTelefono = new JTextField(20);
	
	public AnagraficaPersonaNuovo(int width, int height, String title, AnagraficaPersona parent) {
		this.parent = parent;
		this.width = width;
		this.height = height;
		Toolkit screen = Toolkit.getDefaultToolkit();
		Dimension dim = screen.getScreenSize();
		setSize(width,height);
		setMinimumSize(new Dimension(width,height));
		int widthToUse = (((int) dim.getWidth() - getWidth()) / 2);
		int heightToUse = (((int) dim.getHeight() - getHeight()) / 2);
		setBounds(widthToUse, heightToUse, width, height);
		setTitle(title);
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setVisible(true);
		mainWindow();
	}
	
	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public void mainWindow() {
		LayoutManager flow = new FlowLayout();
		JPanel test1 = new JPanel(flow);
		
		JPanel test2 = new JPanel(flow);
		JPanel test3 = new JPanel(flow);
		JPanel test4 = new JPanel(flow);
		JLabel labelCognome = new JLabel("Cognome:");
		JLabel labelNome = new JLabel("Nome");
		JLabel labelTelefono = new JLabel("Telefono");
		
		
		JButton salva = new JButton("Salva");
		JButton annulla = new JButton("Annulla");
		salva.addActionListener(this);
		annulla.addActionListener(this);
		
		test1.add(labelCognome);
		test1.add(textCognome);
		
		test2.add(labelNome);
		test2.add(textNome);
		
		test3.add(labelTelefono);
		test3.add(textTelefono);
		
		GridLayout grid = new GridLayout(4,1);
		grid.setVgap(0);
		grid.setHgap(0);
		setLayout(grid);
		add(test1);
		add(test2);
		add(test3);
		test4.add(salva);
		test4.add(annulla);
		add(test4);
		
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String source = ((JButton) e.getSource()).getText();
		if (source.equalsIgnoreCase("salva")) {
			String cognome = this.textCognome.getText();
			String nome = this.textNome.getText();
			String telefono = this.textTelefono.getText();
			
			if (nome.equalsIgnoreCase("") || cognome.equalsIgnoreCase("") || telefono.equalsIgnoreCase("")) {
				JOptionPane.showMessageDialog(this, "Informazioni Mancanti");
			} else {
				this.parent.salva(nome, cognome, telefono);
				dispose();
			}
		}
		
		if (source.equalsIgnoreCase("annulla")) {
			dispose();
		}
		
	}	
	

}

package com.izaura.generics.mvn.main;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.WindowConstants;
import javax.swing.table.DefaultTableModel;

import com.izaura.generics.mvn.common.Persona;
import com.izaura.generics.mvn.common.PersonaException;
import com.izaura.generics.mvn.lista.Lista;
import com.izaura.generics.mvn.lista.ListaMain;

public class AnagraficaPersona extends JFrame implements ActionListener {
	private JButton filtra = new JButton("Filtra");
	private JTextField filtraField = new JTextField(20);
	private static final long serialVersionUID = 1L;
	private static final String FILE_RUBRICA = "/tmp/rubrica.txt";
	private JTable grigliaPersone = new JTable();
	private JButton modifica = new JButton("Modifica");
	private JButton nuovo = new JButton("Nuovo");
	private JButton cancella = new JButton("Cancella");
	protected Lista<Persona> listaContatti = new ListaMain<>();
	private String[] etichetteColonne = new String[] {"ID","Cognome: ", "Nome: ", "Telefono: "};
	private JMenuItem menuSalva = new JMenuItem("Salva");
	private JMenuItem menuChiudi = new JMenuItem("Chiudi");

	public AnagraficaPersona(int width, int height, String title) {
		// this.listaContatti = lista;
		caricaDaFile();
		makeWindow(width, height, title);
		this.grigliaPersone.setDefaultEditor(Object.class, null);
		JScrollPane scGrigliaPersone = new JScrollPane(this.grigliaPersone);
		add(scGrigliaPersone);
		makeList(null);
		setVisible(true);
	}

	private void makeWindow(int width, int height, String title) {
		Toolkit screen = Toolkit.getDefaultToolkit();
		Dimension dim = screen.getScreenSize();
		setLayout(new BorderLayout());
		setSize(width, height);
		int widthToUse = (((int) dim.getWidth() - getWidth()) / 2);
		int heightToUse = (((int) dim.getHeight() - getHeight()) / 2);
		setBounds(widthToUse, heightToUse, width, height);
		setTitle(title);
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		makeFilter();
		makePanel();
		makeMenu();
	}

	private void makeFilter() {
		JPanel filter = new JPanel(new FlowLayout());
		filter.add(new JLabel("Filtra per Cognome:"));
		filter.add(this.filtraField);
		filter.add(this.filtra);
		this.filtra.addActionListener(this);
		add(filter, BorderLayout.PAGE_START);
	}
	private void makePanel() {
		JPanel pnlPulsanti = new JPanel();
		pnlPulsanti.setLayout(new FlowLayout());
		this.nuovo.addActionListener(this);
		this.modifica.addActionListener(this);
		this.cancella.addActionListener(this);
		pnlPulsanti.add(this.nuovo);
		pnlPulsanti.add(this.modifica);
		pnlPulsanti.add(this.cancella);
		add(pnlPulsanti, BorderLayout.PAGE_END);
	}

	private void makeMenu() {
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		JMenu menu = new JMenu("Menu");
		menuBar.add(menu);
		this.menuSalva.addActionListener(this);
		this.menuChiudi.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		menu.add(this.menuSalva);
		menu.add(this.menuChiudi);

	}

	private void makeList(String filter) {
		String[][] valori = new String[this.listaContatti.getSize()][4];
		if (filter == null) {
			Integer i = 0;
			for (Persona p : this.listaContatti) {
				valori[i][1] = p.getCognome();
				valori[i][2] = p.getNome();
				valori[i][3] = p.getTelefono();
				valori[i][0] = i.toString();
				i++;
			}
		} else {
			int i = 0;
			int valoriTrovati = 0;
			for (Persona p : this.listaContatti) {
				filter = filter.toUpperCase();
				if (p.getCognome().toUpperCase().startsWith(filter)) {
					valori[i][1] = p.getCognome();
					valori[i][2] = p.getNome();
					valori[i][3] = p.getTelefono();
					valoriTrovati++;
					i++;
				}
			}
			valori = Arrays.copyOf(valori, valoriTrovati);
		}
		DefaultTableModel model = new DefaultTableModel(valori, this.etichetteColonne);
		this.grigliaPersone.setModel(model);
		this.grigliaPersone.getTableHeader().setReorderingAllowed(false);
		this.grigliaPersone.getColumnModel().getColumn(0).setMaxWidth(20);
	}

	public void salva(String nome, String cognome, String telefono) {
		listaContatti.add(new Persona(nome, cognome, telefono));
		makeList(null);
	}

	public void modifica(String nome, String cognome, String telefono, int posizione) {
		System.out.println(listaContatti.get(posizione));
		listaContatti.get(posizione).setEverything(nome, cognome, telefono);
		makeList(null);
	}

	public void caricaDaFile() {
		try (BufferedReader lettore = new BufferedReader(new FileReader(FILE_RUBRICA));) {
			try {

				String riga = lettore.readLine();
				while (null != riga) {
					try {
						Persona np = new Persona(riga);
						listaContatti.add(np);
					} catch (PersonaException e) {
						System.out.println("Persona Exception");
					}
					riga = lettore.readLine();
				}

			} catch (IOException exc) {
				System.out.println(exc.getMessage());
			}
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}

	public void salvaSuFile() {
		try {
			try (BufferedWriter bfw = new BufferedWriter(new FileWriter(FILE_RUBRICA))) {
				for (Persona unaPersona : this.listaContatti) {
					bfw.write(unaPersona.toString());
					bfw.newLine();
				}
				bfw.flush();
				// Close non necessario: Try-catch già lo fa da solo
			}
		} catch (IOException exc) {
			System.out.println(exc.getMessage());
		}
	}

	@Override
	public void actionPerformed(ActionEvent event) {

		if (event.getSource() instanceof JMenuItem) {
			actionMenu(event);
		}

		if (event.getSource() instanceof JButton) {
			actionButton(event);
		}
	}

	private void actionMenu(ActionEvent event) {
		JMenuItem menuClick = (JMenuItem) event.getSource();
		if (menuClick == this.menuSalva) {
			salvaSuFile();
		}
	}

	private void actionButton(ActionEvent event) {
		JButton objClick = (JButton) event.getSource();
		if (objClick == this.nuovo) {
			new AnagraficaPersonaNuovo(400, 400, "Aggiungi nuovo", this);
		}

		if (objClick == this.filtra) {
			makeList(this.filtraField.getText());
		}

		if (objClick == this.modifica) {
			int posizione = (this.grigliaPersone.getSelectedColumn() - 1);
			if (posizione >= 0) {
				new AnagraficaPersonaModifica(400, 400, "Modifica", this, posizione);
			}
		}

		if (objClick == this.cancella) {
			int posizione = Integer.parseInt(this.grigliaPersone.getValueAt(this.grigliaPersone.getSelectedRow(), 0).toString());
			//Object what = this.grigliaPersone.getValueAt(posizione, 3);
			
			if (posizione >= 0) {
				String nome = listaContatti.get(posizione).getNome();
				int conferma = JOptionPane.showConfirmDialog(null, "<html>Sicuro di voler cancellare: <br>" + nome,
						"Sicuro?", JOptionPane.YES_NO_OPTION);
				if (conferma == 0 && posizione >= 0) {
					listaContatti.remove(posizione);
					makeList(null);
				}
			}

		}
	}

}

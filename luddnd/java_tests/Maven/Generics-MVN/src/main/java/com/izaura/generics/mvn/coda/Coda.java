package com.izaura.generics.mvn.coda;

public interface Coda<T> {
	void enqueue(T automobile);
	T dequeue();
}

package com.izaura.generics.mvn.main;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

public class AnagraficaPersonaModifica extends JFrame implements ActionListener{
	private static final long serialVersionUID = 1L;
	private AnagraficaPersona parent = null;
	private int posizione = 0;
	private JTextField textCognome = new JTextField(20);
	private JTextField textNome = new JTextField(20);
	private JTextField textTelefono = new JTextField(20);
	private JButton salva = new JButton("Modifica");
	private JButton annulla = new JButton("Annulla");
	
	public AnagraficaPersonaModifica(int width, int height, String title, AnagraficaPersona parent, int posizione) {
		this.parent = parent;
		this.posizione = posizione;
		putText(posizione);
		Toolkit screen = Toolkit.getDefaultToolkit();
		Dimension dim = screen.getScreenSize();
		setSize(width,height);
		setMinimumSize(new Dimension(width,height));
		int widthToUse = (((int) dim.getWidth() - getWidth()) / 2);
		int heightToUse = (((int) dim.getHeight() - getHeight()) / 2);
		setBounds(widthToUse, heightToUse, width, height);
		setTitle(title);
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setVisible(true);
		mainWindow();
	}
	
	public void putText(int posizione) {
		String cognome = this.parent.listaContatti.get(posizione).getCognome();
		String nome = this.parent.listaContatti.get(posizione).getNome();
		String telefono = this.parent.listaContatti.get(posizione).getTelefono();
		this.textCognome.setText(cognome);
		this.textNome.setText(nome);
		this.textTelefono.setText(telefono);
	}
	public void mainWindow() {
		LayoutManager flow = new FlowLayout();
		JPanel test1 = new JPanel(flow);
		JPanel test2 = new JPanel(flow);
		JPanel test3 = new JPanel(flow);
		JPanel test4 = new JPanel(flow);
		JLabel labelCognome = new JLabel("Cognome:");
		JLabel labelNome = new JLabel("Nome");
		JLabel labelTelefono = new JLabel("Telefono");
		
		this.salva.addActionListener(this);
		this.annulla.addActionListener(this);
		
		test1.add(labelCognome);
		test1.add(textCognome);
		
		test2.add(labelNome);
		test2.add(textNome);
		
		test3.add(labelTelefono);
		test3.add(textTelefono);
		
		GridLayout grid = new GridLayout(4,1);
		grid.setVgap(0);
		grid.setHgap(0);
		setLayout(grid);
		add(test1);
		add(test2);
		add(test3);
		test4.add(this.salva);
		test4.add(this.annulla);
		add(test4);
		
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JButton objClick = (JButton) e.getSource();
		if (objClick == this.annulla) {
			setVisible(false);
		}
		if (objClick == this.salva) {
			String cognome = this.textCognome.getText();
			String nome = this.textNome.getText();
			String telefono = this.textTelefono.getText();
			
			if (nome.equalsIgnoreCase("") || cognome.equalsIgnoreCase("") || telefono.equalsIgnoreCase("")) {
				JOptionPane.showMessageDialog(this, "Informazioni Mancanti");
			} else {
				this.parent.modifica(nome, cognome, telefono, this.posizione);
				dispose();
			}
		}
				
				
	}	
	

}

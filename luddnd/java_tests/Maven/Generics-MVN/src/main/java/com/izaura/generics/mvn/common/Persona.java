package com.izaura.generics.mvn.common;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor

public class Persona {
	private static final String SEPARATORE = ";";
	private String nome;
	private String cognome;
	private String telefono;
	
	public Persona(String myRecord) throws PersonaException{
		if (myRecord != null) {
			String[] campi = myRecord.split(SEPARATORE);
			if (campi.length >= 3) {
				this.nome = campi[0];
				this.cognome = campi[1];
				this.telefono = campi[2];
				return;
			}
		} 
		throw new PersonaException();
	}
	
	public void setEverything(String nome, String cognome, String telefono) {
		this.nome = nome;
		this.cognome = cognome;
		this.telefono = telefono;
	}

	@Override
	public String toString() {
		return String.format("%s%s%s%s%s", this.nome, SEPARATORE, this.cognome, SEPARATORE, this.telefono);
	}
	
	
}

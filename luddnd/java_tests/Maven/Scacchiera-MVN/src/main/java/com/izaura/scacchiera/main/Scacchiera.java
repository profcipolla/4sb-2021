package com.izaura.scacchiera.main;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

public class Scacchiera extends JFrame implements ActionListener{
	private static final long serialVersionUID = 1L;
	private JButton[] bottoni = new JButton[25];

	public Scacchiera(int width,int height) {
		setTitle("Scacchiera");
		Toolkit screen = Toolkit.getDefaultToolkit();
		Dimension dim = screen.getScreenSize();
		setLayout(new BorderLayout());
		setSize(width, height);
		int widthToUse = (((int) dim.getWidth() - getWidth()) / 2);
		int heightToUse = (((int) dim.getHeight() - getHeight()) / 2);
		setBounds(widthToUse, heightToUse, width, height);
		setLayout(new BorderLayout());
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		setResizable(false);
		makeWindow();
		setVisible(true);
	}
	
	private void makeWindow() {
		JPanel pane = new JPanel(new GridLayout(5,5));
		pane.setSize(300, 300);
		for (int i = 0; i <= 24; i++) {
			bottoni[i] = new JButton();
			bottoni[i].addActionListener(this);
			if (i % 2 != 0) {
				bottoni[i].setName("w");
				bottoni[i].setBackground(new Color(255,255,255));
			} else {
				bottoni[i].setName("Black");
				bottoni[i].setBackground(new Color(0,0,0));
			}
			pane.add(bottoni[i]);
		}
		add(pane, BorderLayout.CENTER);
	}

	public static void main(String[] args) {
		new Scacchiera(600,600);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JButton source = (JButton) e.getSource();
		Icon img = new ImageIcon("/src/main/resources/regina.png");
		source.setIcon(img);
	}

}

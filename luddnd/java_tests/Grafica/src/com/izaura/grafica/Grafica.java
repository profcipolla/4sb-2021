package com.izaura.grafica;

import java.awt.BasicStroke;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

public class Grafica extends JFrame{
	private static final long serialVersionUID = 1L;
	private static final int WIDTH = 640;
	private static final int HEIGHT = 480;
	private static final int WIDTH_START = WIDTH/2-200;
	private static final int HEIGHT_START = HEIGHT/2-200;
	
	public Grafica() {
		setSize(WIDTH,HEIGHT);
		setTitle("Grafica");
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		setVisible(true);
	}
	@Override
	public void paint(Graphics g2) {
		Graphics2D g = (Graphics2D) g2;
		//g.setStroke(new BasicStroke(3));
		
		
		BufferedImage myImage = new BufferedImage(WIDTH,HEIGHT, BufferedImage.TYPE_INT_RGB);
		
		Graphics2D gImage = myImage.createGraphics();
		gImage.setStroke(new BasicStroke(3));
		gImage.drawLine(WIDTH_START, HEIGHT/2, WIDTH/2+200, HEIGHT/2);
		gImage.drawLine(WIDTH/2, HEIGHT_START, WIDTH/2, HEIGHT/2+200);
		g.setStroke(new BasicStroke(3));
		
		int rgb = 0xFF0000;
		Double xs,ys,y;
		
		double x = -200;
		while(x < 200) {
			y = 100 * Math.cos(0.05 * x);
			xs = (double) (640/2 + x);
			ys = (double) (480/2 - y);
			
			try {
				myImage.setRGB(xs.intValue(), ys.intValue(), rgb);
				g.drawImage(myImage,0,0,null);
			} catch (Exception exc){
				System.out.println(exc.getMessage());
			}
			
			x = x + 0.2;
		}
		
	}
}

import java.util.Scanner;

public class Altezze {

	public static boolean run() {
		// Dato un array con le altezze (in cm) di 10 persone, contane quanti sono
		// + alti di 180cm
		// e quanti hanno un altezza comprese tra 160 cm e 169 cm.
		Scanner tastiera = new Scanner(System.in);
		final int SIZE = 5;
		int alti180 = 0;
		int altinormali = 0;
		int gnomi = 0;
		int[] arr = new int[SIZE];
		for (int i = 0; i <= SIZE-1; i++) {
			System.out.println("Inserisci la " + (i+1) + "° persona");
			arr[i] = tastiera.nextInt();
			if (arr[i] >= 180) {
				alti180++;
			} else {
				if (arr[i] >= 160 && arr[i] <= 170) {
					altinormali++;
				} else {
					gnomi++;
				}
			}
		}
		tastiera.close();
		System.out.println(String.format("Ci sono %d persone più alte di 180cm %d alte tra 160 e 169cm, e %d più basse di 160cm.", alti180, altinormali, gnomi));
		return true;
	}
}

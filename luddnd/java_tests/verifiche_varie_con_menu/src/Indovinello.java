import java.util.Scanner;

public class Indovinello {

	public static boolean run() {
		Scanner attesa = new Scanner(System.in);

		System.out.println("Pensa a un numero.");
		attesa.nextLine();
		System.out.println("Ok, raddoppialo");
		attesa.nextLine();
		int casuale = (int) (Math.random() * 20) + 1;
		System.out.println("Somma " + (casuale * 2));
		attesa.nextLine();
		System.out.println("Dividi per 2");
		attesa.nextLine();
		System.out.println("Sottrai il numero che avevi pensato");
		attesa.nextLine();
		System.out.println("Hai ottenuto " + casuale);
		// System.out.println("Ho generato il numero: " + casuale);
		attesa.close();
		return true;
	}
}

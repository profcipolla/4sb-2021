import java.util.Scanner;

public class TrovaNumero {

	public static boolean run() {
		// TODO Dato un array di numeri e letto un valore N, trovare la posizine
		// dell'ultimo vaolre N,
		// nell'interno dell'array (-1 se N non è presente)
		final int SIZE = 10;
		int posizione = -1;
		int arr[] = new int[SIZE];
		for (int i = 0; i <= SIZE-1; i++) {
			arr[i] = (int)(Math.random()*100+1);
		}
		
		//Sotto il randomizer per debug
		Scanner tastiera = new Scanner(System.in);
		System.out.println("Insersci il numero da trovare");
		int input = tastiera.nextInt();
		
		for (int i = 0; i <= SIZE-1; i++) {
			if (arr[i] == input) {
				posizione = i;
			}
		}
		
		if (posizione == -1) {
			System.out.println("Numero non trovato");
		} else {
			System.out.println("Numero trovato in posizione: " + posizione);
		}
		tastiera.close();
		return true;
	}
}

import java.util.Scanner;

public class Scomposizione {
	
	public static boolean run() {
		Scanner tastiera = new Scanner(System.in);
		System.out.println("Inserisci numero da scomporre.");
		int numero = tastiera.nextInt();
		int divisore = 2;
		while (divisore <= numero) {
			if (numero % divisore == 0) {
				System.out.println(divisore);
				numero = numero / divisore;
			} else {
				divisore++;
			}
		}
		tastiera.close();
		return true;
	}

}

import java.util.Scanner;

public class SommaTraAeB {

	public static boolean run() {
		// Leggere due interi A e B.
		// Sommare i valori pari tra A e B. (A deve essere più piccolo di B)

		Scanner tastiera = new Scanner(System.in);
		System.out.println("Inserisci il primo numero");
		int A = tastiera.nextInt();
		System.out.println("Inserisci il secondo");
		int B = tastiera.nextInt();
		int somma = 0;
		if (A >= 0 && B >= 0) {
			if (B < A) {
				int scambio = B;
				B = A;
				A = scambio;
			}
			if (A % 2 != 0) {
				A++;
			}
			for (int i = A; i <= B; i = i + 2) {
				somma = somma + i;
			}
			System.out.println(String.format("La somma dei numeri è: %d", somma));
		} else {
			System.out.println("Numeri non validi");
		}
		tastiera.close();
		return true;
	}
}

import java.util.Scanner;

public class Main {
	
	// TODO dato un array controlla se è palindromo (A specchio)
	// TODO Fai un programma per fare le tabbelline della tombola

	public static void main(String[] args) {
		Scanner tastiera = new Scanner(System.in);
		System.out.println("Programmi: ");
		System.out.println("1: Somma Fino A Zero");
		System.out.println("2: Scomposizione");
		System.out.println("3: Media dei numeri");
		System.out.println("4: Indovino un numero");
		System.out.println("5: Somma tra A e B");
		System.out.println("6: Tabellina del 2");
		System.out.println("7: Tabellina del 7 rovesciata");
		System.out.println("8: Trova un numero");
		System.out.println("9: Altezze");
		int input = tastiera.nextInt();
		while (!directory(input)) {
			System.out.println("Programma non valido");
			input = tastiera.nextInt();
		}
		tastiera.close();
	}

	public static boolean directory(int input) {
		// Restituisce true se ha trovato un programma
		switch (input) {
		case 1:
			return SommaFinoAZero.run();
		case 2:
			return Scomposizione.run();
		case 3:
			return MediaNumeri.run();
		case 4:
			return Indovinello.run();
		case 5:
			return SommaTraAeB.run();
		case 6:
			return Tabellina2.run();
		case 7:
			return Tabellina7.run();
		case 8:
			return TrovaNumero.run();
		case 9:
			return Altezze.run();
		default:
			return false;
		}
	}

}
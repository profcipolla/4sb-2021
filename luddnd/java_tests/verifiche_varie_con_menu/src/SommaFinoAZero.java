import java.util.Scanner;

public class SommaFinoAZero {
	
	public static boolean run() {
		Scanner tastiera = new Scanner(System.in);
		System.out.println("Inserisci un numero, scrivi 0 per fermarlo.");
		int numero = tastiera.nextInt();
		int risultato = 0;

		while (numero != 0) {
			risultato = risultato + numero;
			System.out.println("Ok, inseriscine un altro o premi 0.");
			System.out.println("Per ora siamo a: " + risultato);
			numero = tastiera.nextInt();
		}
		System.out.println("Risultato finale: " + risultato);
		tastiera.close();
		return true;
	}

}

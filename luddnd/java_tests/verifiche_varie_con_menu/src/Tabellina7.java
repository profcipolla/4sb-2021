
public class Tabellina7 {

	public static boolean run() {
		int size = 10;
		int array[] = new int[size];
		int j = 0;
		System.out.println("Eccoti la tabellina del 7 rovesciata");
		for (int i = size - 1; i >= 0; i--) {
			array[j] = (i + 1) * 7;
			System.out.println(array[j]);
			j++;
		}
		return true;
	}
}

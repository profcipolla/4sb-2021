import java.util.Scanner;

public class MediaNumeri {

	public static boolean run() {
		// Leggo dei numeri da tastiera finchè non viene inserito 0
				// Calcolo e scrivo la media dei valori inseriti
				int media = 0;
				Scanner tastiera = new Scanner(System.in);
				int totaleNumeri = 0;
				float risultato;
				System.out.println("Inserisci numeri da fare la media, premi 0 per fermare");
				int input = tastiera.nextInt();
				/*
				 * do { input = tastiera.nextInt(); if (input != 0) { media = media + input;
				 * totaleNumeri++; System.out.println("Aggiunto numero: " + totaleNumeri); } }
				 * while (input != 0);
				 */

				while (input != 0) {
					media = media + input;
					totaleNumeri++;
					input = tastiera.nextInt();
				}

				// Controllo se non ho 0 siccome non posso dividere per 0
				if (totaleNumeri == 0) {
					risultato = 0;
				} else {
					risultato = (1.0f * media) / totaleNumeri;
				}
				tastiera.close();
				System.out.println(String.format("La media dei %d valori è: %f", totaleNumeri, risultato));
				return true;
	}
}

package com.izaura.common;

public class Automobile {
	private String targa;
	
	public Automobile() {
		
	}

	@Override
	public String toString() {
		return String.format("Automobile targa: %s", this.targa);
	}

	public String getTarga() {
		return targa;
	}

	public void setTarga(String targa) {
		this.targa = targa;
	}

	public Automobile(String targa) {
		this.targa = targa;
	}

}

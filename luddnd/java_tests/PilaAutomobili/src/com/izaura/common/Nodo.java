package com.izaura.common;

public class Nodo {
	private Automobile info;
	private Nodo next;
	
	public Nodo(Automobile info, Nodo next) {
		super();
		this.info = info;
		this.next = next;
	}
	
	public Nodo(Automobile info) {
		super();
		this.info = info;
		this.next = null;
	}
	
	
	public Automobile getInfo() {
		return info;
	}
	
	public void setInfo(Automobile info) {
		this.info = info;
	}
	public Nodo getNext() {
		return next;
	}
	public void setNext(Nodo next) {
		this.next = next;
	}
	
	
	
}

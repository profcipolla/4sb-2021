package com.izaura.common;

import com.izaura.coda.*;
import com.izaura.pila.*;

public class Main {

	public static void main(String[] args) {
		Pila miaPila = new PilaMain();
		Coda miaCoda = new CodaMain();
		
		miaPila.push(new Automobile("DAbabyCar"));
		miaPila.push(new Automobile("DaortherBabyCAr"));
		miaPila.push(new Automobile("vroom"));
		//System.out.println(miaPila.getSize());
		
		Automobile a = miaPila.pop();
		//System.out.println(a);
		a = miaPila.pop();
		//System.out.println(a);
		//System.out.println(miaPila.getSize());
		
		
		miaCoda.enqueue(new Automobile("Automobile1"));
		miaCoda.enqueue(new Automobile("Automobile2"));
		miaCoda.enqueue(new Automobile("Automobile3"));
		miaCoda.enqueue(new Automobile("Automobile4"));
		
		System.out.println(miaCoda.dequeue());
		System.out.println(miaCoda.dequeue());
		System.out.println(miaCoda.dequeue());
		System.out.println(miaCoda.dequeue());
	}

}

package com.izaura.coda;

import com.izaura.common.Automobile;

public interface Coda {
	void enqueue(Automobile automobile);
	Automobile dequeue();
}

package com.izaura.coda;

import com.izaura.common.Automobile;
import com.izaura.common.Nodo;

// FIFO - First in, first out
public class CodaMain implements Coda{
	private Nodo tail = null;

	@Override
	public void enqueue(Automobile automobile) {
		this.tail = new Nodo(automobile,this.tail);
	}

	@Override
	public Automobile dequeue() {
		if (null == this.tail) {
			return null;
		}
		
		Nodo tmp = this.tail;
		Nodo prec = null;
		while (tmp.getNext() != null) {
			prec = tmp;
			tmp = tmp.getNext();
		}		
		
		Automobile auto = tmp.getInfo();
		if (prec == null) {
			this.tail = null;
		} else {
			prec.setNext(null);
		}
		return auto;
	}
}

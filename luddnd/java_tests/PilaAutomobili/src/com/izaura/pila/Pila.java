package com.izaura.pila;

import com.izaura.common.Automobile;

public interface Pila {
	Automobile pop();
	void push(Automobile automobile);
	int getSize();

}

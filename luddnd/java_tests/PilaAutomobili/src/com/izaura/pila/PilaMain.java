package com.izaura.pila;

import com.izaura.common.Automobile;
import com.izaura.common.Nodo;

public class PilaMain implements Pila{
	//LIFO - Last in First Out
	private Nodo top = null;
	private int size = 0;

	@Override
	public Automobile pop() {
		if (null == top) {
			return null;
		}
		Automobile auto = this.top.getInfo();
		this.top = this.top.getNext();
		size--;
		return auto;
	}

	@Override
	public void push(Automobile automobile) {
		Nodo newNodo = new Nodo(automobile, this.top);
		this.top = newNodo;
		size++;
		
	}

	@Override
	public int getSize() {
		return size;
	}
	

}

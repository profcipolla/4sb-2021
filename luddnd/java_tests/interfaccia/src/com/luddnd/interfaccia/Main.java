package com.luddnd.interfaccia;

public class Main {

	public static void main(String[] args) {
		Automobile auto = new Automobile("Fiat", "AB66CC");
		Persona mario = new Persona("Rossi", "Mario");
		Universita Sapienza = new Universita("Sapienza");
		
		Stampante s = new Stampante();
		s.eseguiStampa(auto);
		s.eseguiStampa(mario);
		s.eseguiStampa(Sapienza);
		s.eseguiReset(mario);
		s.eseguiStampa(mario);
	}

}

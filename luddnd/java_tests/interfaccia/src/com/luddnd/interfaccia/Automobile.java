package com.luddnd.interfaccia;

public class Automobile implements Stampabile{
	private String modello;
	private String targa;
	
	public Automobile(String modello, String targa) {
		this.targa = targa;
		this.modello = modello;
	}

	@Override
	public void stampa() {
		System.out.println(
				String.format("Modello: %s, targa: %s", this.modello, this.targa)
		);
	}

}

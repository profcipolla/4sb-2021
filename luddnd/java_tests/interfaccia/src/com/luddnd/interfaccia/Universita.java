package com.luddnd.interfaccia;

public class Universita implements Stampabile{
	private String nome;
	
	public Universita(String nome) {
		this.nome = nome;
	}
	
	public void stampa() {
		System.out.println(String.format("Nome universitÓ: %s ", this.nome));
	}
}

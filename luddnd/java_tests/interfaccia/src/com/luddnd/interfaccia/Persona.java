package com.luddnd.interfaccia;

public class Persona implements Stampabile, Cancellabile {
	
	private String nome;
	private String cognome;
	
	public Persona(String nome, String cognome) {
		this.nome = nome;
		this.cognome = cognome;
	}

	@Override
	public void stampa() {
		System.out.println(
				String.format("Cognome: %s, nome: %s", this.cognome, this.nome));
	}

	@Override
	public void reset() {
		this.nome = "";
		this.cognome = "";
		
	}
}

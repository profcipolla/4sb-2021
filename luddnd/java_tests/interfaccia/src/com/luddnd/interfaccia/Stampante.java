package com.luddnd.interfaccia;

public class Stampante {
	public void eseguiStampa(Stampabile oggetto) {
		oggetto.stampa();
	}
	public void eseguiReset(Cancellabile oggetto) {
		oggetto.reset();
	}
}

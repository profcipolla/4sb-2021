import java.util.Scanner;

public class CalcolaMedia {

	
	// Leggo dei numeri da tastiera finché non viene 
	// inserito lo zero, calcolo e scrivo la media
	// di valori inseriti
	public static void main(String[] a) {
		Scanner tastiera = new Scanner(System.in);
		Integer somma = 0;
		Integer conta = 0;
		
		System.out.print("Inserisci un numero: ");
		Integer n = tastiera.nextInt();
		
		while(n != 0) {
			somma = somma + n;
			conta++;
			
			System.out.print("Inserisci un numero: ");
			n = tastiera.nextInt();
		}
		
		Float risultato =  (1.0f * somma) / conta;
		
		System.out.println(String.format("La media dei %d valori è: %#.2f", conta, risultato));
		
	}
	
	
}

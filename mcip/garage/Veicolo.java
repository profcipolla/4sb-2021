public class Veicolo {
   private String marca;
   private int anno;
   private int cilindrata;
   
   public Veicolo(String marca, int anno, int cilindrata) {
       this.setMarca(marca);
       this.setAnno(anno);
       this.setCilindrata(cilindrata);
   }
      
   public String getMarca() {
       return this.marca;
   }
   
   public int getAnno() {
       return this.anno;
   }
   
   public int getCilindrata() {
       return this.cilindrata;
   }
   
   public void setMarca(String marca) {
       this.marca = marca;
   }

   public void setAnno(int anno) {
       this.anno = anno;
   }
   
   public void setCilindrata(int cilindrata) {
       this.cilindrata = cilindrata;
   }
   
   public String toString() {
        return "Marca: " + this.getMarca() + ", Anno: " + this.getAnno() + ", Cilindrata: " + this.getCilindrata();
   }
}

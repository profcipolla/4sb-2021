public class Garage {
    private Veicolo[] posti = new Veicolo[15];
    
    public boolean immettiVeicolo(Veicolo veicoloInIngresso) {
        int pos = -1;
        int i = 0;
        while (i < 15 && -1 == pos) {
            if (null == posti[i]) {
                pos = i;
            }
            // i = i + 1;
            i++;
        }
        
        if (-1 == pos) {
            return false;
        }
        
        posti[pos] = veicoloInIngresso;
        return true;
    }
    
    public Veicolo estraiVeicolo (int posizione) {
        Veicolo v = posti[posizione];
        posti[posizione] = null;
        return v;
    }
    
    public void stampa() {
        for (int i = 0; i <= 14; i++) {
            if (null == posti[i]) {
                System.out.println("La posizione " + i + " non è occupata");
            } else {
                System.out.println(posti[i].toString());
            }
        }
    }
}

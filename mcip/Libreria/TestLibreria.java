
public class TestLibreria {
  public Libreria libreria;

  public void esegui() {
       this.libreria = new Libreria();
       this.libreria.nome = "Libreria Scolastica Armellini";
       this.libreria.indirizzo = "Piazza Beato Placido Riccardi";
   
       Scaffale s1 = new Scaffale();
       s1.piano = 1;
       s1.stanza = "Stanza Blue";
       s1.posizione = "Entrando subito a sinistra";
       
       Scaffale s2 = new Scaffale();
       s2.piano = 1;
       s2.stanza = "Stanza Verde";
       s2.posizione = "In fondo a destra";
       
       this.libreria.scaffali = new Scaffale[100];
       this.libreria.scaffali[0] = s1;
       this.libreria.scaffali[1] = s2;
       
       this.libreria.scaffali[2] = new Scaffale();
       this.libreria.scaffali[2].piano = 2;
       this.libreria.scaffali[2].stanza = "Stanza buia";
       this.libreria.scaffali[2].posizione = "A destra";
       
       this.libreria.scaffali[3] = new Scaffale();
       Scaffale appoggio = this.libreria.scaffali[3];
       this.libreria.scaffali[3].piano = 2;
       appoggio.stanza = "Stanza Test";
       appoggio.posizione = "Sulla parete verde";
       
       Scaffale s4 = new Scaffale();
       
       Libro l1 = new Libro("I promessi sposi", 1860, "ABCD1234");
       l1.autore = new Autore();
       l1.autore.nome = "Alessandro";
       l1.autore.cognome = "Manzoni";
       l1.autore.annoNascita = 1843;
       
       Libro l2 = new Libro("La colonna infame", 1864, "DEFGh789", l1.autore);
       
       Libro l3 = new Libro("La realtà non è come appare", 2018, "UUUUHHH3333", new Autore("Carlo", "Rovelli", 1967));
       
       s1.libri = new Libro[200];
       s1.libri[0] = l1;
       s1.libri[1] = l2;
       s1.libri[2] = l3;
       
       l1.autore.nazione = "Italia";
       
  }
    
}

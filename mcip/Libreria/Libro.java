public class Libro
{
    public String titolo;
    public int annoPubblicazione;
    public String codiceISBN;
    
    public Autore autore;
    
    public Libro(String titolo, int annoPubblicazione, String codiceISBN) {
        this.titolo = titolo;
        this.annoPubblicazione = annoPubblicazione;
        this.codiceISBN = codiceISBN;
    }
    
    public Libro(String titolo, int annoPubblicazione, String codiceISBN, Autore autore) {
        this.titolo = titolo;
        this.annoPubblicazione = annoPubblicazione;
        this.codiceISBN = codiceISBN;
        this.autore = autore;
    }
}

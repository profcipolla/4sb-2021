public class Autore {
    public String nome;
    public String cognome;
    public int annoNascita;
    public String nazione;
    
    public Autore() {
        
    }
    
    public Autore(String nome, String cognome, int annoNascita) {
        this.nome = nome;
        this.cognome = cognome;
        this.annoNascita = annoNascita;
    }
}

public class Universita {
    public final int NUMERO_STUDENTI = 1000;
    public final int NUMERO_CORSI = 20;
    public final int MIN_NUMERO_DOCENTI = 20;
    
    private String nome;
    private Sede sede;
    private Studente[] studenti = new Studente[NUMERO_STUDENTI];
    private Corso[] corsi = new Corso[NUMERO_CORSI];
    private Docente[] docenti;
    
    public Universita(String nome, Sede sede, int numeroDocenti) {
        this.nome = nome;
        this.sede = sede;
        if (numeroDocenti <= 0) {
            numeroDocenti = MIN_NUMERO_DOCENTI;
        }
        docenti = new Docente[numeroDocenti];
    }
  

    public boolean aggiungiStudente(Studente studente) {
        int i = 0;
          
        // && = AND, || = OR
        while(i < NUMERO_STUDENTI) {
            if (null == studenti[i]) {
               this.studenti[i] = studente;
               return true;
            } else if (studente.equals(studenti[i])) {
                System.out.println("Studente già presente");
                return false;
            }
            i++;
        }
        System.out.println("Iscrizione non disponibile per raggiunto limite studenti");
        return false;
    }
    
    public boolean aggiungiStudenti(Studente[] tantiStudenti) {
        
        return true;
    }
    
    public boolean aggiungiCorso(Corso nuovoCorso) {
        int i = 0;
        
        // il ciclo while si ripete mentre la condizione è vera!
        // in questo caso la condizione è composta con un AND.
        while(i < NUMERO_CORSI && null != this.corsi[i]) {
            i++;
        }
        
        if (i >= NUMERO_CORSI) {
            // corsi disponibili esauriti
            return false;
        }
        
        this.corsi[i] = nuovoCorso;
        return true;
    }
    
    public boolean aggiungiDocente(Docente nuovoDocente) {
        
        return true;
    }
    
    public boolean aggiungiFrequenza(Corso corso, Studente studente) {
        boolean inserito = corso.aggiungiStudente(studente);
        return inserito;
    }
    
    public boolean aggiungiDocente(Corso corso, Docente docente) {
        return corso.aggiungiDocente(docente);
    }
    
    public String getNome() {
        return this.nome;
    }
    
    public boolean verificaStudente(Studente studente) {
        // TODO
        return true;
    }
    
    public boolean verificaCorso(Corso corso) {
        // TODO
        return true;
    }
    
    public int numeroStudenti(Corso corso) {
        return 0;
    }
    
}
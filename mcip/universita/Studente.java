public class Studente {
    private String nome;
    private String cognome;
    
    public Studente() {
        this("", "");
    }
    
    public Studente(String nome, String cognome) {
        this.setNome(nome);
        this.setCognome(cognome);
    }
    
    public String getNome() {
        return this.nome;
    }
    
    public String getCognome() {
        return this.cognome;
    }
    
    public void setNome(String nome) {
        if (null == nome) {
            this.nome = "";
        } else {
            this.nome = nome.trim();
        }
    }
    
    public void setCognome(String cognome) {
        if (null == cognome) {
            this.cognome = "";
        } else {
            this.cognome = cognome.trim();
        }
    }
    
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Studente)) {
            return false;
        }
        
        /*
        if (null == this.nome || null == this.cognome) {
            return false;
        }
        */
       
        Studente otherStudente = (Studente)other;
        
        return this.nome.equalsIgnoreCase(otherStudente.getNome()) 
            && this.cognome.equalsIgnoreCase(otherStudente.getCognome());
    }
}

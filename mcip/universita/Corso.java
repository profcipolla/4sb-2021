public class Corso {
    public String nome;
    
    public Studente[] studenti = new Studente[50];
    public Docente docente;
    
    public Corso(String nome) {
        this.nome = nome;
    }
    
    public String getNome() {
        return this.nome;
    }
    
    public void setNome(String nome) {
        this.nome = nome;
    }
    
    public boolean aggiungiStudente(Studente nuovoStudente) {
        int i = 0;
        while(i < 50 && null != studenti[i]) {
            i++;
        }
        
        if (i >= 50) {
            return false;
        }
        
        this.studenti[i] = nuovoStudente;
        return true;
    }
    
    public boolean aggiungiDocente(Docente nuovoDocente) {
        if (this.docente != null) {
            return false;
        } 
        
        this.docente = nuovoDocente;
        return true;
    }
    
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Corso)) {
            return false;
        }
        
        if (null == this.nome) {
            return false;
        }
        
        Corso otherCorso = (Corso)other;
        
        return this.nome.equals(otherCorso.getNome());
    }
}

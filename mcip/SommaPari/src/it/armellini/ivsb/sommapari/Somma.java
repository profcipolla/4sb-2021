package it.armellini.ivsb.sommapari;

import java.util.Scanner;

public class Somma {

	public static void main(String[] args) {
		Scanner t = new Scanner(System.in);
		System.out.print("Inserisci il primo numero: ");
		Integer a = t.nextInt();

		System.out.print("Inserisci il secondo numero: ");
		Integer b = t.nextInt();

		if (a > b) {
			int tmp = a;
			a = b;
			b = tmp;
		}

		if (a % 2 == 0) {
			a = a + 2; // a += 2;
		} else {
			a = a + 1; // a++;
		}

		int s = 0;
		for (int i = a; i < b; i += 2) {
			s = s + i;
		}

		System.out.print(String.format("La somma dei valori pari tra %d e %d è %d", a, b, s));

		t.close();
	}

}

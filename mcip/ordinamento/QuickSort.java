package ordinamento;

public class QuickSort {

	private int[] unArray;

	public QuickSort(int[] loArray) {
		unArray = new int[loArray.length];

		for (int i = 0; i < loArray.length; i++) {
			unArray[i] = loArray[i];
		}
	}

	public void sort() {
		this.sort(0, unArray.length - 1);
	}
	
	private void sort(int inf, int sup) {
		int pos = partizionamento(inf, sup);
		if (inf < pos - 1) {
			sort(inf, pos - 1);
		}
		if (pos + 1 < sup) {
			sort(pos + 1, sup);
		}
	}
	
	private int partizionamento(int inf, int sup) {
		int posPivot = inf;
		int i = posPivot + 1;
		int s = sup;

		while (i <= s) {
			while (unArray[s] > unArray[posPivot]) {
				s--;
			}

			while (i < s && unArray[i] <= unArray[posPivot]) {
				i++;
			}

			if (i < s) {
				int appo = unArray[i];
				unArray[i] = unArray[s];
				unArray[s] = appo;
				i++;
				s--;
			}
		}

		// scambio il pivot
		int appo = unArray[posPivot];
		unArray[posPivot] = unArray[s];
		unArray[s] = appo;

		return s;
	}

}

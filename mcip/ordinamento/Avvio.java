package ordinamento;

public class Avvio {

	public static void main(String[] args) {
		int[] esempioArray = new int[] {7, 3, 17, 23, 4, 99, 5, 6, 29, 1, 98};
		
		QuickSort qs = new QuickSort(esempioArray);
		qs.sort();
		
		BubbleSort bs = new BubbleSort(esempioArray);
		bs.sort();
		
		
	}

}

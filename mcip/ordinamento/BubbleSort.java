package ordinamento;

public class BubbleSort {
	private int[] unArray;

	public BubbleSort(int[] unArray) {
		// prepara lo spazio per ospitare gli elementi di unArray
		this.unArray = new int[unArray.length];

		for (int i = 0; i < unArray.length; i++) {
			this.unArray[i] = unArray[i];
		}
	}

	public void sort() {
		int x = this.unArray.length - 1 ;

		for (int i = 0; i < this.unArray.length - 1; i++) {
			x--;
			for (int j = 0; j <= x; j++) {
				if (this.unArray[j] > this.unArray[j + 1]) {
					int appo = this.unArray[j];
					this.unArray[j] = this.unArray[j + 1];
					this.unArray[j + 1] = appo;
				}
			}
		}
	}
}

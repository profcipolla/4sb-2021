package it.armellini.ivsb.gestione.eccezioni;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Avvio {

	public static void main(String[] args) {
		Scanner t = new Scanner(System.in);
		int[] numeri = new int[10];
		
		System.out.print("Per favore inserisci un numero: ");
		
		try {
			Integer valore = t.nextInt();
			int i;
	
			for (i = 0; i < valore; i++) {
				System.out.println("Leggo la pos " + i + " valore " + numeri[i]);
			}
			System.out.println("Fuori dal for " + i + " valore " + numeri[i]);
			
			
			double inverso = 1.0d / valore;
			System.out.println(String.format("L'inverso di %d è %f", valore, inverso));
			
		// TODO: si possono intercettare tutte e due le exception
		// con
		// catch(Exception exc) {
		// ...
		} catch(InputMismatchException exc) {
			System.out.println("Errore durante l'acquisizione del valore");
			// System.out.println(exc.getMessage());
			// exc.printStackTrace();
		} catch(ArrayIndexOutOfBoundsException exc1) {
			System.out.println("Errore accedendo all'array");
			System.out.println(exc1.getMessage());
		} catch(Exception exc) {
			System.out.println("Tutti gli altri casi");
		} finally {
			System.out.println("Queste righe le vedrai sempre");
		}

		System.out.println("Chiudo la tastiera");
		t.close();
	}

}

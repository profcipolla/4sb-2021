package it.armellini.ivsb.struttura;

import java.util.Iterator;

public class PilaImpl<T> implements Pila<T> {

	private Nodo<T> head = null;
	
	public void push(T info) {
		/*
		Nodo<T> nn = new Nodo<T>(info, null);
		if (null == this.head) {
			this.head = nn;
		} else {
			nn.setSucc(this.head);
			this.head = nn;
		}
		*/
		this.head = new Nodo<T>(info, this.head);
	}

	public T pop() {
		if (null != this.head) {
			T info = this.head.getInfo();
			this.head = this.head.getSucc();
			return info;
		}
		return null;
	}

	public Iterator<T> iterator() {

		return null;
	}

}

package it.armellini.ivsb.struttura;

public interface List<T> extends Iterable<T> {
	void add(T info);
	int size();
	
	T get(int posizione);
	
	// cancella l'elemento in posizione "posizione" dalla lista
	// si inizia a contare da zero
	void remove(int posizione);
}

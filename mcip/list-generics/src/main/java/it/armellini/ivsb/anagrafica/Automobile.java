package it.armellini.ivsb.anagrafica;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Automobile {
	private String modello;
	private String targa;
}

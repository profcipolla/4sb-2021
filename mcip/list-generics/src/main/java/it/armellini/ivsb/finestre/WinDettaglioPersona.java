package it.armellini.ivsb.finestre;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class WinDettaglioPersona extends JFrame implements ActionListener {
	private WinListaPersona winListaPersona = null;

	private JButton btnSalva = new JButton("Salva");
	private JButton btnAnnulla = new JButton("Annulla");

	private JTextField txtNome = new JTextField(40);
	private JTextField txtCognome = new JTextField(40);
	private JTextField txtTelefono = new JTextField(40);

	public WinDettaglioPersona(String title, WinListaPersona winListaPersona) {
		this.winListaPersona = winListaPersona;

		setBounds(100, 100, 600, 400);
		setTitle(title);

		setLayout(new BorderLayout());

		JPanel pnlText = new JPanel();
		pnlText.setLayout(new GridLayout(3, 2));
		pnlText.add(new JLabel("Nome:"));
		txtNome.setBounds(10,  10, 600, 40);
		pnlText.add(txtNome);
		pnlText.add(new JLabel("Cognome:"));
		pnlText.add(txtCognome);
		pnlText.add(new JLabel("Telefono:"));
		pnlText.add(txtTelefono);

		add(pnlText, BorderLayout.CENTER);

		JPanel pnlBottoni = new JPanel();
		pnlBottoni.setLayout(new FlowLayout());
		pnlBottoni.add(btnSalva);
		btnSalva.addActionListener(this);
		
		pnlBottoni.add(btnAnnulla);
		btnAnnulla.addActionListener(this);

		add(pnlBottoni, BorderLayout.SOUTH);

		setVisible(true);
	}

	public void actionPerformed(ActionEvent ae) {
		JButton btnClicked = (JButton)ae.getSource();
		
		// TODO se il pulsante premuto è salva
		if (btnClicked == this.btnSalva) {
			String nome = this.txtNome.getText();
			String cognome = this.txtCognome.getText();
			String telefono = this.txtTelefono.getText();
	
			if (null == nome || "".equals(nome.trim())) {
				JOptionPane.showMessageDialog(this, "Per favore inserisci il nome.");
				return;
			}
			
			if (null == cognome || "".equals(cognome.trim())) {
				JOptionPane.showMessageDialog(this, "Per favore inserisci il cognome.");
				return;
			}
			
			if (null == telefono || "".equals(telefono.trim())) {
				JOptionPane.showMessageDialog(this, "Per favore inserisci il telefono.");
				return;
			}

			this.winListaPersona.salva(nome, cognome, telefono);
		} else if (btnClicked == this.btnAnnulla) {
			// TODO: se il pulsante premuto è annulla
			this.setVisible(false);
		}
	}


}

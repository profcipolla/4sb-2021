package it.armellini.ivsb.struttura;

public interface Coda<T> extends Iterable<T> {
	void enqueue(T info);
	
	T dequeue();
}

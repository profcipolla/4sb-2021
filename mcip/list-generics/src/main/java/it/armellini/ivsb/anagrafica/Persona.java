package it.armellini.ivsb.anagrafica;

import it.armellini.ivsb.errori.PersonaException;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Persona {
	private static final String SEPARATORE = ";";
	
	private String nome;
	private String cognome;
	private String telefono;
	
	public Persona(String record) throws PersonaException {
		if (null != record) {
			String[] campi = record.split(SEPARATORE);
			if (campi.length >= 3) {
				this.nome = campi[0];
				this.cognome = campi[1];
				this.telefono = campi[2];
				return;
			}
		}
		throw new PersonaException();
	}
	
	@Override
	public String toString() {
		return String.format("%s%s%s%s%s", this.nome, SEPARATORE, this.cognome, SEPARATORE, this.telefono );
	}
}

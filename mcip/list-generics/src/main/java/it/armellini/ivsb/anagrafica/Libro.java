package it.armellini.ivsb.anagrafica;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Libro {
	private String titolo;
	private int numPagine;
}

package it.armellini.ivsb.finestre;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class WinProva extends JFrame implements ActionListener {
	
	JButton btnOk = new JButton("OK");
	JButton btnAnnulla = new JButton("Annulla");
	
	public WinProva() {
		setTitle("Finestra di prova");
		setBounds(50, 50, 800, 600);
		setLayout(new FlowLayout());
		add(this.btnOk);
		btnOk.addActionListener(this);
		add(this.btnAnnulla);
		btnAnnulla.addActionListener(this);
		setVisible(true);
	}

	public void actionPerformed(ActionEvent arg0) {
		JButton btn = (JButton)arg0.getSource();
		if (btn == btnOk) {
			JOptionPane.showMessageDialog(this, "Hai premuto ok");
		} else if (btn == btnAnnulla) {
			System.exit(0);
		}
		
	}
	

}

package it.armellini.ivsb.finestre;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;

public class WinAnagraficaPersona extends JFrame implements WindowListener {
	private static final long serialVersionUID = 1L;

	private JTable tabPersone = new JTable(20, 5);
			
	public WinAnagraficaPersona() {
		setBounds(40, 40, 400, 600);
		setTitle("Gestione Persone");
		addWindowListener(this);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
		JScrollPane jsp = new JScrollPane(tabPersone);
		add(jsp);
		
		setVisible(true);
	}

	public void windowActivated(WindowEvent arg0) {
		System.out.println("Sei nell'evento Activated");
	}

	public void windowClosed(WindowEvent arg0) {
		System.out.println("Sei nell'evento Closed");
	}

	public void windowClosing(WindowEvent arg0) {
		// JOptionPane jp = ne
		System.out.println("Sei nell'evento Closing");
	}

	public void windowDeactivated(WindowEvent arg0) {
		System.out.println("Sei nell'evento Deactivate");
	}

	public void windowDeiconified(WindowEvent arg0) {
		System.out.println("Sei nell'evento Deiconified");
	}

	public void windowIconified(WindowEvent arg0) {
		System.out.println("Sei nell'evento Iconified");
	}

	public void windowOpened(WindowEvent arg0) {
		System.out.println("Sei nell'evento Opended");
	}
}

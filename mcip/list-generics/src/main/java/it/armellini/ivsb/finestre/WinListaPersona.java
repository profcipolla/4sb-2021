package it.armellini.ivsb.finestre;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import it.armellini.ivsb.anagrafica.Persona;
import it.armellini.ivsb.errori.PersonaException;
import it.armellini.ivsb.struttura.List;
import it.armellini.ivsb.struttura.ListImpl;

public class WinListaPersona extends JFrame implements ActionListener {
	private static final String FILE_RUBRICA = "/tmp/rubrica.txt";

	private List<Persona> listaPersone;
	private JTable grigliaPersone = new JTable();

	private JButton btnNuovo = new JButton("Nuovo");
	private JButton btnModifica = new JButton("Modifica");
	private JButton btnCancella = new JButton("Cancella");

	private JMenuItem miSalva = new JMenuItem("Salva");
	private JMenuItem miChiudi = new JMenuItem("Chiudi");

	private String[] etichetteColonne = new String[] { "", "Cognome", "Nome", "Telefono" };

	private WinDettaglioPersona winDettaglioPersona = null;

	public WinListaPersona() {
		setTitle("Rubrica");
		setBounds(50, 50, 800, 900);
		this.creaMenu();

		setLayout(new BorderLayout());

		JScrollPane scGrigliaPersone = new JScrollPane(this.grigliaPersone);
		add(scGrigliaPersone, BorderLayout.CENTER);
		this.caricaDaFile();
		this.showData();

		JPanel pnlPulsanti = new JPanel();
		pnlPulsanti.setLayout(new FlowLayout());
		pnlPulsanti.add(this.btnNuovo);
		this.btnNuovo.addActionListener(this);

		pnlPulsanti.add(this.btnModifica);
		pnlPulsanti.add(this.btnCancella);
		this.btnCancella.addActionListener(this);

		add(pnlPulsanti, BorderLayout.PAGE_END);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
	}

	private void creaMenu() {
		JMenuBar bar = new JMenuBar();
		setJMenuBar(bar);
		JMenu mnuFile = new JMenu("File");
		bar.add(mnuFile);

		mnuFile.add(miSalva);
		miSalva.addActionListener(this);
		mnuFile.add(new JSeparator());
		mnuFile.add(miChiudi);
		miChiudi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				System.exit(0);
			}
		});
	}

	public void salva(String nome, String cognome, String telefono) {
		System.out.println(String.format("Devo salvare %s, %s, %s", nome, cognome, telefono));
		this.listaPersone.add(new Persona(nome, cognome, telefono));
		this.showData();
	}

	private void showData() {
		String[][] valori = new String[listaPersone.size()][4];
		int i = 0;
		for (Persona p : listaPersone) {
			valori[i][0] = "" + i;
			valori[i][1] = p.getCognome();
			valori[i][2] = p.getNome();
			valori[i][3] = p.getTelefono();
			i++;
		}

		DefaultTableModel model = new DefaultTableModel(valori, this.etichetteColonne);
		this.grigliaPersone.setModel(model);
	}

	public void actionPerformed(ActionEvent ae) {
		// click sulle voci di menu..
		if (ae.getSource() instanceof JMenuItem) {
			System.out.println("Hai premuto il menu Salva");
			this.salvaSuFile();
			// click sui bottoni...
		} else {
			JButton objClick = (JButton) ae.getSource();

			if (objClick == this.btnNuovo) {
				if (null != winDettaglioPersona) {
					this.winDettaglioPersona.setVisible(true);
					this.winDettaglioPersona.setTitle("Nuovo Contatto");
				} else {
					this.winDettaglioPersona = new WinDettaglioPersona("Nuovo Contatto", this);
				}
			} else if (objClick == this.btnModifica) {
				if (null != winDettaglioPersona) {
					this.winDettaglioPersona.setVisible(true);
					this.winDettaglioPersona.setTitle("Modifica Contatto");
				} else {
					this.winDettaglioPersona = new WinDettaglioPersona("Modifica Contatto", this);
				}
			} else if (objClick == this.btnCancella) {
				int rigaSelezionata = this.grigliaPersone.getSelectedRow();
				if (rigaSelezionata >= 0) {
					System.out.println("Sto per cancellare la riga " + rigaSelezionata);
					this.listaPersone.remove(rigaSelezionata);
					this.showData();
				}
			}
		}
	}

	private void caricaDaFile() {
		this.listaPersone = new ListImpl<Persona>();
		try {
			BufferedReader bfr = new BufferedReader(new FileReader(FILE_RUBRICA));
			String riga = bfr.readLine();
			while(null != riga) {
				// trasformo la riga del file in un oggetto di tipo Persona
				Persona np = new Persona(riga);
				this.listaPersone.add(np);
				riga = bfr.readLine();
			}
			bfr.close();
		} catch (IOException exc) {
			System.out.println("Errore durante l'apertura del file: " + exc.getMessage());
		} catch (PersonaException e) {
			System.out.println("Dati del file non validi");
		}
	}

	private void salvaSuFile() {
		try {
			try (BufferedWriter bfw = new BufferedWriter(new FileWriter(FILE_RUBRICA))) {
				for (Persona unaPersona : listaPersone) {
					bfw.write(unaPersona.toString());
					bfw.newLine();
				}
				bfw.flush();
			}
		} catch (IOException exc) {
			System.out.println(exc.getMessage());
		}
	}
}

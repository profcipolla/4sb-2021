package it.armellini.ivsb.struttura;

/**
 * Collezione di tipo LIFO, ovvero l'ultimo che entra
 * è il primo che esce (Last In, First Out)
 * 
 * @author john
 *
 * @param <T>
 */
public interface Pila<T> extends Iterable<T>{
	void push(T info);
	
	T pop ();
}

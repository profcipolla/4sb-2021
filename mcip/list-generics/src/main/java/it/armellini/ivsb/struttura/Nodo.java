package it.armellini.ivsb.struttura;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Nodo<T> {
	private T info;
	private Nodo<T> succ;
	
	public Nodo() {
		this.info = null;
		this.succ = null;
	}
}

package it.armellini.ivsb.struttura;

import java.util.Iterator;

public class ListImpl<T> implements List<T> {
	private Nodo<T> head = null;
	private int numElementi = 0;

	public void add(T info) {
		numElementi++;
		this.head = new Nodo<T>(info, this.head);
	}

	public T get(int posizione) {
		Nodo<T> copia = this.head;
		while (0 != posizione && null != copia) {
			copia = copia.getSucc();
			posizione--;
		}

		if (null != copia) {
			return copia.getInfo();
		}
		return null;
	}

	public Iterator<T> iterator() {
		return new IteratorList<T>(this.head);
	}

	private class IteratorList<TP> implements Iterator<TP> {
		private Nodo<TP> nodoCorrente;

		public IteratorList(Nodo<TP> head) {
			this.nodoCorrente = head;
		}

		public boolean hasNext() {
			return null != this.nodoCorrente;

			/*
			 * if (null != this.nodoCorrente) { return true; } else { return false; }
			 */
		}

		public TP next() {
			TP info = this.nodoCorrente.getInfo();
			this.nodoCorrente = this.nodoCorrente.getSucc();
			return info;
		}

	}

	public int size() {
		return this.numElementi;
	}

	public void remove(int posizione) {
		if (posizione < 0 || posizione > numElementi - 1) {
			return;
		}
		if (0 == posizione) {
			this.head = this.head.getSucc();
			this.numElementi--;
			return;
		}
		
		Nodo<T> temp = this.head;
		int conta = 0;

		// && = AND ovvero devono essere vere entrambi le condizioni
		while (null != temp && conta != (posizione - 1)) {
			temp = temp.getSucc();
			// conta++ è come conta = conta + 1, ovvero aggiungo
			// uno a conta e rimetto il nuovo valore dentro conta
			conta++;
		}

		if (conta == (posizione - 1)) {
			if (null != temp.getSucc()) {
				temp.setSucc(temp.getSucc().getSucc());
			} else {
				this.head = null	;
			}
			this.numElementi--;
		}
	}

}

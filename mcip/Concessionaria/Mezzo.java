
public abstract class Mezzo
{
    public CasaCostruttrice casaCostruttrice;
    public String modello;
    public int annoDiCostruzione;
    
    public Mezzo(CasaCostruttrice casaCostruttrice, String modello, int annoDiCostruzione) {
        this.casaCostruttrice = casaCostruttrice;
        this.modello = modello;
        this.annoDiCostruzione = annoDiCostruzione;
    }
     
}

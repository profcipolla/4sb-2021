

public class Motoveicolo extends Mezzo
{
    private int numeroCilindri;
    private int numeroMarce;
    
    public Motoveicolo(CasaCostruttrice casaCostruttrice, String modello, int annoDiCostruzione, int numeroCilindri) {
        super(casaCostruttrice, modello, annoDiCostruzione);
        this.numeroCilindri = numeroCilindri;
    
    }
}

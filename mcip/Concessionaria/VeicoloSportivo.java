public class VeicoloSportivo extends Autoveicolo
{
    public int velocita;
    
    public VeicoloSportivo (CasaCostruttrice casaCostruttrice, String modello, int annoDiCostruzione, String colore, int velocita) {
        super(casaCostruttrice, modello, annoDiCostruzione, colore);
        this.velocita = velocita;
    }
}

public class Autoveicolo extends Mezzo
{
    public String colore;
    public double prezzoEuro;
    
    
    public Autoveicolo (CasaCostruttrice casaCostruttrice, String modello, int annoDiCostruzione, String colore) {
        super(casaCostruttrice, modello, annoDiCostruzione);
        this.colore = colore;
    }
}

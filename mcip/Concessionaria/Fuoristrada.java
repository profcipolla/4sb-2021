public class Fuoristrada extends Autoveicolo
{
    public String tipoTrazione;
    public int numeroMarce;
    
    public Fuoristrada (CasaCostruttrice casaCostruttrice, String modello, int annoDiCostruzione, String colore, String tipoTrazione) {
        super(casaCostruttrice, modello, annoDiCostruzione, colore);
        this.tipoTrazione = tipoTrazione;
        this.numeroMarce = 5;
    }
    
    public Fuoristrada (CasaCostruttrice casaCostruttrice, String modello, int annoDiCostruzione, String colore, String tipoTrazione, int numeroMarce) {
        super(casaCostruttrice, modello, annoDiCostruzione, colore);
        this.tipoTrazione = tipoTrazione;
        this.numeroMarce = numeroMarce;
    }
   
}

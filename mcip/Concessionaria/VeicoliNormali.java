public class VeicoliNormali extends Autoveicolo
{
    private int numeroPosti;
    
    public VeicoliNormali (CasaCostruttrice casaCostruttrice, String modello, int annoDiCostruzione, String colore, int numeroPosti) {
        super(casaCostruttrice, modello, annoDiCostruzione, colore);
        this.numeroPosti = numeroPosti;
    }
}


public class CasaCostruttrice
{
    // attributi
    public String nome;
    public String nazione;
    public int annoFondazione;
    public int numeroConcessionarieInItalia;

    // costruttori
    public CasaCostruttrice(String nome, String nazione) {
        this.nome = nome;
        this.nazione = nazione;
    }
    
    public CasaCostruttrice(String pNome, String pNazione, int pAnnoFondazione, int pNumeroConcessionarieInItalia) {
        nome = pNome;
        nazione = pNazione;
        annoFondazione = pAnnoFondazione;
        numeroConcessionarieInItalia = pNumeroConcessionarieInItalia;
    }
    
    // metodi
}

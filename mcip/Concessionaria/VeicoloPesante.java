

public class VeicoloPesante extends Mezzo
{
    public int massaKg;
    public int portataKg;
    
    public VeicoloPesante(CasaCostruttrice casaCostruttrice, String modello, int annoDiCostruzione) {
        super(casaCostruttrice, modello, annoDiCostruzione);
        massaKg = 12000;
        portataKg = 25000;
    }
    
        public VeicoloPesante(CasaCostruttrice casaCostruttrice, String modello, int annoDiCostruzione, int massaLKg, int portataKg) {
        super(casaCostruttrice, modello, annoDiCostruzione);
        this.massaKg = massaKg;
        this.portataKg = portataKg;
    }
}

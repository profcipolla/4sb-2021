
public class Moto extends Veicolo {
    // peso in KG
    private int peso;
    private int numeroCilindri;
    
    public Moto(String marca, int anno, int cilindrata, int peso, int numeroCilindri) {
        super(marca, anno, cilindrata);
        this.setPeso(peso);
        this.setNumeroCilindri(numeroCilindri);
    }
    
    
    public void setPeso(int peso) {
        this.peso = peso;
    }
    
    public void setNumeroCilindri(int numeroCilindri) {
        this.numeroCilindri = numeroCilindri;
    }

    public int getPeso() {
        return this.peso;
    }
    
    public int getNumeroCilindri() {
        return numeroCilindri;
    }
    
    public String toString() {
        return super.toString() + ", Peso: " + peso + ", Numero Cilindri: " + this.numeroCilindri;
    }
}

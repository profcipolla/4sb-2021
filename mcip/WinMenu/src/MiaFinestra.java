import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JSeparator;

public class MiaFinestra extends JFrame implements ActionListener {
	// private static final long serialVersionUID = 1L;

	public MiaFinestra(String titolo) {
		setTitle(titolo);
		setSize(600, 400);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JMenuBar mb = new JMenuBar();
		JMenu mFile = new JMenu("File");
		mb.add(mFile);
		JMenu mImpostazioni = new JMenu("Impostazioni");
		mb.add(mImpostazioni);
		JMenu mInformazioni = new JMenu("Informazioni");
		mb.add(mInformazioni);
	
		JMenuItem miApri = new JMenuItem("Apri");
		miApri.addActionListener(this);
		mFile.add(miApri);
		mFile.add(new JSeparator());
		JMenuItem miSalva = new JMenuItem("Salva");
		miSalva.addActionListener(this);
		mFile.add(miSalva);
		JMenuItem miSalvaCome = new JMenuItem("Salva Come...");
		miSalvaCome.addActionListener(this);
		mFile.add(miSalvaCome);
		mFile.add(new JSeparator());
		JMenuItem miChiudi = new JMenuItem("Chiudi");
		miChiudi.addActionListener(this);
		mFile.add(miChiudi);
		
		
		JMenuItem miAbout = new JMenuItem("About");
		miAbout.addActionListener(this);
		mInformazioni.add(miAbout);
		
		setJMenuBar(mb);
		setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent ae) {
		JMenuItem itemClicked = (JMenuItem) ae.getSource();
		String scritta = itemClicked.getText();

		System.out.println("Hai cliccato " + scritta);
	
		if ("About".equals(scritta)) {
			JOptionPane.showMessageDialog(this, "Mia Applicazione v. 1.0.0");
		} else if ("Chiudi".equals(scritta)) {
			System.exit(0);
		}
		
		
	}
	
	
	public static void main(String[] a) {
		new MiaFinestra("Prova Menu Java Swing");
	}
}

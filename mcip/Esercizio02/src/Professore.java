import java.util.Scanner;

public class Professore {

	public static void main(String[] args) {
		Scanner tastiera = new Scanner(System.in);
		System.out.print("Per favore inserisci un voto (1..10)");

		int v = tastiera.nextInt();
		
		if (v >= 6) {
			// qui istr vero
			System.out.println("Promosso");
		} else {
			// qui istr falso
			System.out.println("Bocciato");
		}

	}

}

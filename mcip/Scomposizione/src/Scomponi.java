import java.util.Scanner;

public class Scomponi {
	
	public static void main(String[] args) {
		// letto un numero intero N, trovare i fattori che
		// lo compongono
		
		Scanner tst = new Scanner(System.in);
		
		System.out.print("Inserisci un numero intero positivo: ");
		Integer n = tst.nextInt();
		int divisore = 2;
		
		System.out.print("La scomposizione di " + n + " è");
		while(n > 1) { 
			// controlla se n è divisibile per divisore
			if (n % divisore == 0) {
				System.out.print("  " + divisore);
				// effettuo la divisione
				n = n / divisore;
			} else {
				// proverà con un altro divisore
				divisore++;
			}
		}
		
		
	}
	
}

import java.util.Scanner;

public class Indovina {
	public static void main(String[] a) {
		Scanner tst = new Scanner(System.in);
		System.out.print("Pensa un numero intero. Premi invio quando hai fatto.");
		tst.nextLine();
		System.out.print("Raddoppia il numero. Premi invio quando hai fatto.");
		tst.nextLine();
		int casuale = (int) (Math.random() * 20) + 1;
		System.out.print("Somma " + (casuale * 2) + ". Premi invio quando hai fatto.");
		tst.nextLine();
		System.out.print("Dividi per due. Premi invio quando hai fatto.");
		tst.nextLine();
		System.out.print("Sottrai il numero che avevi pensato all'inizio. Premi invio quando hai fatto.");
		tst.nextLine();
		System.out.println("Hai ottenuto " + casuale);
	}
}

public class Automobile {
    // litri su km
    private float consumoDiCarburante;
    private float quantitaCarburante = 0;
    
    
    public Automobile(float consumoDiCarburante) {
        this.consumoDiCarburante = consumoDiCarburante;
        this.quantitaCarburante = 0;
    }

    public boolean guida(float distanzaKm) {
        float consumo = this.consumoDiCarburante * distanzaKm;
        if (consumo > quantitaCarburante) {
            return false;
        } else {
            this.quantitaCarburante = this.quantitaCarburante - consumo;
            return true;
        }
    }
    
    public float dammiCarburante() {
        return this.quantitaCarburante;
    }
    
    public void faiRifornimento(float litri) {
        this.quantitaCarburante = this.quantitaCarburante + litri;
    }
}

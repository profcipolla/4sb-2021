package it.armellini.ivsb.esercizio03;

public class TavolaPitagorica {

	public static void main(String[] args) {
		// for(inizializzazione;test;incremento)
		
		/*
		for (int r = 1; r <= 10; r = r + 1) {
			for (int i = 1; i <= 10; i = i + 1) { // i++, i += 1
				System.out.print("  " + (i * r));
			}
			System.out.println();
		}
		*/
		
		// for (int c = 1; c <= 10; c++)
		int c = 1;
		while(c <= 10) {
			System.out.print("  " + c);
			++c;
		}
		
		System.out.println();
		
		c = 11;
		do {
			System.out.print("  " + c);
			c++;
		} while(c <= 10);
	}

}

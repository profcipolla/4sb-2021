public class Professore extends Persona {
    // dati
    private String telefono;
    
    public Studente primoDellaClasse;
    
    
    
    // costruttori
    
    public Professore() {
        
    }
    
    
    public Professore(String nome, String cognome, Studente primoDellaClasse) {
        this.nome = nome;
        this.cognome = cognome;
        this.primoDellaClasse = primoDellaClasse;
    }
    
    
    // azioni
    public void stampaDatiStudente() {
        System.out.println("Nome Studente " + this.primoDellaClasse.nome);
        System.out.println("Cognome Studente " + this.primoDellaClasse.cognome);
    }
    
    
}

import java.util.Date;

public class Studente extends Persona {
   // dati
   public int matricola;
   public Date dataNascita;
   
   // costruttori
   public Studente() {
       this.nome = "Sconosciuto";
       this.cognome = "Sconosciuto";
   }
   
   public Studente(String nome, String cognome) {
       this.nome = nome;
       this.cognome = cognome;
   }
   
   public Studente(int matricola, String nome, String cognome) {
       this.matricola = matricola;
       this.nome = nome;
       this.cognome = cognome;
   }
   
   // metodi (azioni)
}

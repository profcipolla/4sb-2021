import java.util.Scanner;

public class Sommatore {
	public static void main(String[] args) {
		Scanner tastiera = new Scanner(System.in);

		System.out.print("Per favore inserisci un numero: ");
		Long n = tastiera.nextLong();

		Long s = 0l;

		// while ripete mentre la condizione è vera
		while (n != 0l) {
			s = s + n; // s += n;

			System.out.print("Per favore inserisci un numero: ");
			n = tastiera.nextLong();
		}

		System.out.println("La somma dei numeri inseriti è: " + s);
		tastiera.close();
	}
}

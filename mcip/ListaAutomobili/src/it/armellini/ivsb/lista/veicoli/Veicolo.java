package it.armellini.ivsb.lista.veicoli;

public class Veicolo {
	private String targa;
	
	public Veicolo(String targa) {
		this.targa = targa;
	}

	public String getTarga() {
		return targa;
	}

	public void setTarga(String targa) {
		this.targa = targa;
	}
	
	@Override
	public String toString() {
		return String.format("Targa: %s", this.targa);
	}
}

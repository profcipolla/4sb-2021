package it.armellini.ivsb.lista.struttura;

import it.armellini.ivsb.lista.veicoli.Veicolo;

public interface Lista {
	void add(Veicolo veicolo);
	Veicolo get(int posizione);
	Veicolo search(String targa);
	void remove(String targa);
	void printToOut();
	int size();
}

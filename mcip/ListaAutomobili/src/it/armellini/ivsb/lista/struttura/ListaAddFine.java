package it.armellini.ivsb.lista.struttura;

import it.armellini.ivsb.lista.veicoli.Veicolo;

public class ListaAddFine implements Lista {
	private Nodo head = null;
	
	@Override
	public void add(Veicolo veicolo) {
		Nodo nn = new Nodo(veicolo, null);
		if (null == this.head) {
			this.head = nn;
		} else {
			Nodo temp = this.head;
			while(null != temp.getSucc() ) {
				temp = temp.getSucc();
			}
			temp.setSucc(nn);
		}
	}

	@Override
	public Veicolo get(int posizione) {
		Nodo copia = this.head;
		while(0 != posizione && null != copia) {
			copia = copia.getSucc();
			posizione--;
		}
		
		if (null != copia) {
			return copia.getInfo();
		}
		return null;
	}

	@Override
	public Veicolo search(String targa) {
		if (null == targa) {
			return null;
		}
		
		Nodo temp = this.head;
		
		while (null != temp && !targa.equalsIgnoreCase(temp.getInfo().getTarga())) {
			temp = temp.getSucc();
		}

		if (null == temp) {
			return null;
		}
		return temp.getInfo();
	}

	@Override
	public void remove(String targa) {
		// la cancellazione avvine ridefinendo il puntamento
		// al successivo, in modo da lasciare fuori l'elemento che si vuole
		// cancellare.
		
		if (null == this.head || null == targa) {
			return;
		}
		
		Nodo temp = this.head;
		Nodo tempPrec = null;
		while (null != temp && !targa.equalsIgnoreCase(temp.getInfo().getTarga())) {
			tempPrec = temp;
			temp = temp.getSucc();
		}
		
		if (null == temp) {
			return;
		}
		
		if (null == tempPrec) {
			this.head = this.head.getSucc();
		} else {
			tempPrec.setSucc(temp.getSucc());
		}
		
	}

	@Override
	public void printToOut() {
		Nodo temp = this.head;
		
		while (null != temp) {
			System.out.println("Veicolo " + temp.getInfo());
			temp = temp.getSucc();
		}
		
	}

}

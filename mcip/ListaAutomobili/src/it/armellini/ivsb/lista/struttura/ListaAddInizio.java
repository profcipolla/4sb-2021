package it.armellini.ivsb.lista.struttura;

import it.armellini.ivsb.lista.veicoli.Veicolo;

public class ListaAddInizio implements Lista {
	private Nodo head = null;
	
	public void add (Veicolo newVeicolo) {
		Nodo newNodo = new Nodo(newVeicolo, this.head);
		this.head = newNodo;
	}
	
	public Veicolo get(int posizione) {
		
		Nodo copia = this.head;
		while(0 != posizione && null != copia) {
			copia = copia.getSucc();
			posizione--;
		}
		
		if (null != copia) {
			return copia.getInfo();
		}
		return null;
	}
	
	public Veicolo search(String targa) {

		return null;
	}

	@Override
	public void remove(String targa) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void printToOut() {
		// TODO Auto-generated method stub
		
	}
}

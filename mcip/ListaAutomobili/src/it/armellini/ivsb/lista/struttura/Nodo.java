package it.armellini.ivsb.lista.struttura;

import it.armellini.ivsb.lista.veicoli.Veicolo;

public class Nodo {
	private Veicolo info;
	private Nodo succ;

	public Nodo(Veicolo info, Nodo succ) {
		this.info = info;
		this.succ = succ;
	}

	public Veicolo getInfo() {
		return info;
	}

	public void setInfo(Veicolo info) {
		this.info = info;
	}

	public Nodo getSucc() {
		return succ;
	}

	public void setSucc(Nodo succ) {
		this.succ = succ;
	}

}

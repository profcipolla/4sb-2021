import it.armellini.ivsb.lista.struttura.Lista;
import it.armellini.ivsb.lista.struttura.ListaAddFine;
import it.armellini.ivsb.lista.veicoli.Automobile;
import it.armellini.ivsb.lista.veicoli.Camion;
import it.armellini.ivsb.lista.veicoli.Moto;
import it.armellini.ivsb.lista.veicoli.Veicolo;

public class Avvio {

	public static void main(String[] args) {
		Lista miaLista = new ListaAddFine();
		
		miaLista.add(new Camion("CC123NN"));
		miaLista.add(new Camion("CC456NN"));
		miaLista.add(new Moto("MM000AA"));
		miaLista.add(new Automobile("AA111BB"));
		
		miaLista.printToOut();
		
		Veicolo s = miaLista.search("ZZZZZZ");
		
		if (null == s) {
			System.out.println("Targa ZZZZZZ non presente");
		} else {
			System.out.println("Trovato veicolo:" + s);
		}
		
		s = miaLista.search("MM000AA");
		if (null == s) {
			System.out.println("Targa MM000AA non presente");
		} else {
			System.out.println("Trovato veicolo:" + s);
		}
		
		miaLista.remove("MM000AA");
		miaLista.printToOut();
		
		miaLista.remove("CC123NN");
		miaLista.printToOut();
		
		miaLista.remove("AA111BB");
		miaLista.printToOut();
		
		
		miaLista.remove("CC456NN");
		miaLista.printToOut();
		
		/*
		for (int i = 1; i <= 10; i++) {
			miaLista.add(new Veicolo(String.format("VV0%dVV", i)));
		}
		
		Veicolo p0 = miaLista.get(0);
		System.out.println(p0);
		
		Veicolo p10 = miaLista.get(10);
		System.out.println(p10);
		
		Veicolo p110 = miaLista.get(110);
		System.out.println(p110);
		*/
	}

}

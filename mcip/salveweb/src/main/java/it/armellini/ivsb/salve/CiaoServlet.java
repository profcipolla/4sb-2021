package it.armellini.ivsb.salve;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class CiaoServlet
 */
@WebServlet("/ciao")
public class CiaoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	private static final String HTML_SALVE = "<html>"
			+ "<head>"
			+ " <title>Salve Web</title>"
			+ "</head>"
			+ "<body>"
			+ "	<h1>Test Servlet</h1>"
			+ " <form method='post'>"
			+ "  Inserisci il tuo nome:"
			+ "  <input type='text' name='nomeUtente' placeholder='Qui il nome' />"
			+ "  <br />Inserisci il tuo cognome:"
			+ "  <input type='text' name='cognomeUtente' placeholder='Qui il cognome' />"
			+ "	 <input type='reset' value='Pulisci' />"
			+ "	 <input type='submit' value='Invia' />"
			+ " </form>"
			+ "</body>"
			+ "</html>";
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CiaoServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 * request: mi permette di accedere ai dati del browser
	 * response: permette d'inviare i dati al browser (normalmente una nuova pagina html)
	 * 
	 */
    @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		PrintWriter out = response.getWriter();
	
		out.write(HTML_SALVE);
		out.flush();
		out.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
    @Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String nomeUtente = req.getParameter("nomeUtente");
		
		PrintWriter out =  resp.getWriter();
		out.write("Salve " + nomeUtente);
		out.flush();
		out.close();
	}

}
